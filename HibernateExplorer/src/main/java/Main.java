
import ModelsSQL.Models.*;
import ModelsSQL.DAO.DAO;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Locale;
import java.util.ResourceBundle;


public class Main extends Application{

    @Override   //blok inicjujący przed uruchomieniem aplikacji
    public void init() {

    }

    @Override   //wszystkie czynności kończące pracę aplikacji
    public void stop() {
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/Fxml/Main.fxml"));
        ResourceBundle resourceBundle = ResourceBundle.getBundle("bundles.messages");
        loader.setResources(resourceBundle);

        BorderPane borderPane = loader.load();
        primaryStage.setTitle(resourceBundle.getString("title"));
        primaryStage.getIcons().add(new Image("/icons/wheel.png"));
        primaryStage.setScene(new Scene(borderPane));
        primaryStage.initStyle(StageStyle.DECORATED);
        primaryStage.show();


        hibernateInitialize();

    }

    public void hibernateInitialize() {


        DAO.saveOrUpdateEntity(new CarSQL("GD423","Fiat", "500", CarBodyType.HATCHBACK, LocalDate.of(2016,1,1), "Beżowy", 7653l));
        DAO.saveOrUpdateEntity(new CarSQL("SD7654","Audi", "80", CarBodyType.SEDAN, LocalDate.of(1994,1,1), "Czarny", 338000));
        DAO.saveOrUpdateEntity(new CarSQL("GF8876","BMW", "e60", CarBodyType.SEDAN, LocalDate.of(2004,1,1), "Srebrny", 284000));
        DAO.saveOrUpdateEntity(new CarSQL("GD987876","Skoda", "Octavia", CarBodyType.COMBI, LocalDate.of(2010,1,1), "Szary", 186200));
        DAO.saveOrUpdateEntity(new CarSQL("LK9877", "Audi", "Q8", CarBodyType.SUV, LocalDate.of(2018,1,1), "Czerwony", 15990));
        DAO.saveOrUpdateEntity(new CarSQL("JH87666", "Subaru", "Forester", CarBodyType.SUV, LocalDate.of(2007,1,1), "Czarny", 189313));

        DAO.saveOrUpdateEntity(new AutoRepairShopSQL("Aries Motor Sp. z o.o.","ul. Arkuszowa 11 01-934 Warszawa, woj. mazowieckie",1456234563));
        DAO.saveOrUpdateEntity(new AutoRepairShopSQL("Serwis Fabiańczyk ","ul. Prosta 2c 05-092 Łomianki, woj. mazowieckie",7658765439L));
        DAO.saveOrUpdateEntity(new AutoRepairShopSQL("Sowier Sp. z o.o. ","ul. Wrocławska 97 30-017 Kraków, woj. małopolskie",5646234563L));
        DAO.saveOrUpdateEntity(new AutoRepairShopSQL("Auto Plus","ul. Wałbrzyska 2 02-739 Warszawa, woj. mazowieckie",1987234563));
        DAO.saveOrUpdateEntity(new AutoRepairShopSQL("Auto Serwis Głęboccy","Stary Rynek Oliwski 2 80-324 Gdańsk, woj. pomorskie",7456098563L));
        DAO.saveOrUpdateEntity(new AutoRepairShopSQL("Auto-Naprawa","ul. Sandomierska 2 84-300 Lębork, woj. pomorskie",4566234563L));

        DAO.saveOrUpdateEntity(new MechanicSQL("Joanna","Marciniak",78071212312L));
        DAO.saveOrUpdateEntity(new MechanicSQL("Krzysztof","Zawadzki",55040987656L));
        DAO.saveOrUpdateEntity(new MechanicSQL("Krystyna","MŁot",89021432343L));
        DAO.saveOrUpdateEntity(new MechanicSQL("Tomasz","Zagraj",90050234567L));
        DAO.saveOrUpdateEntity(new MechanicSQL("Emilia","Rataj",84110291234L));
        DAO.saveOrUpdateEntity(new MechanicSQL("Paweł","Duliński",68112102312L));
        DAO.saveOrUpdateEntity(new MechanicSQL("Gniewomir","Kowalski",78071287312L));
        DAO.saveOrUpdateEntity(new MechanicSQL("Katrzyna","Stanisiak",58042412312L));
        DAO.saveOrUpdateEntity(new MechanicSQL("Stanisław","Grot",80063412312L));
        DAO.saveOrUpdateEntity(new MechanicSQL("Piotr","Majewski",89072348312L));
        DAO.saveOrUpdateEntity(new MechanicSQL("Andrzej","Rudy",78092319912L));

        DAO.addRelation_AutoRepairShop_Mechanic(1L,1L);
        DAO.addRelation_AutoRepairShop_Mechanic(1L,2L);
        DAO.addRelation_AutoRepairShop_Mechanic(2L,3L);
        DAO.addRelation_AutoRepairShop_Mechanic(2L,4L);
        DAO.addRelation_AutoRepairShop_Mechanic(3L,5L);
        DAO.addRelation_AutoRepairShop_Mechanic(3L,6L);
        DAO.addRelation_AutoRepairShop_Mechanic(4L,7L);
        DAO.addRelation_AutoRepairShop_Mechanic(4L,8L);
        DAO.addRelation_AutoRepairShop_Mechanic(5L,9L);
        DAO.addRelation_AutoRepairShop_Mechanic(5L,10L);
        DAO.addRelation_AutoRepairShop_Mechanic(5L,11L);

        DAO.saveOrUpdateEntity(setOrder(1L,1L,1L));
        DAO.saveOrUpdateEntity(setOrder(1L,2L,2L));
        DAO.saveOrUpdateEntity(setOrder(2L,3L,3L));
        DAO.saveOrUpdateEntity(setOrder(2L,3L,4L));
        DAO.saveOrUpdateEntity(setOrder(3L,4L,5L));
        DAO.saveOrUpdateEntity(setOrder(4L,7L,6L));
    }

    private RepairOrderSQL setOrder(long autoRepairShopID, long mechanicID, long carID) {
        AutoRepairShopSQL autoRepairShopSQL = DAO.getEntityById(autoRepairShopID, AutoRepairShopSQL.class).get();
        MechanicSQL mechanicSQL = DAO.getEntityById(mechanicID, MechanicSQL.class).get();
        CarSQL carSQL = DAO.getEntityById(carID, CarSQL.class).get();
        String description = "Zlecenie naprawy";
        RepairOrderSQL repairOrderSQL = new RepairOrderSQL(description, false, carSQL, autoRepairShopSQL, mechanicSQL);
        return repairOrderSQL;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
