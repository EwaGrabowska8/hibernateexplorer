package ModelsSQL.DAO;

import ModelsFx.AutoRepairShopFx;
import ModelsFx.CarFx;
import ModelsFx.MechanicFx;
import ModelsSQL.Util.HibernateUtil;
import ModelsSQL.Models.*;
import javafx.scene.control.Toggle;
import lombok.extern.java.Log;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.NoResultException;
import javax.persistence.RollbackException;
import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Level;

@Log
public class DAO {


    public static boolean saveOrUpdateEntity(BaseEntity baseEntity){
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

        Transaction transaction = null;
        try(Session session = sessionFactory.openSession()){

            transaction = session.beginTransaction();
            session.saveOrUpdate(baseEntity);
            transaction.commit();

            return true;
        }catch (HibernateException | IllegalStateException | RollbackException e){

            log.log(Level.SEVERE,"Error saving object.");
            if (transaction!=null){
                transaction.rollback();
            }
        }
        return false;
    }
    public static <T> Optional<T> getEntityById(long id, Class<T> tClass){
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

        T singleResult = null;
        try(Session session = sessionFactory.openSession()){
            String className = tClass.getSimpleName();
            Query<T> query = session.createQuery("from " + className + " where id=" + id, tClass);
            singleResult = query.getSingleResult();
        }catch (NoResultException e){
            log.log(Level.SEVERE,"No result");
        }catch (HibernateException he){
            log.log(Level.SEVERE,"Error login");
        }
        return Optional.of(singleResult);
    }
    public static <T> List<T> getAll(Class<T> tClass){
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        List<T> resultList = new ArrayList<>();
        try(Session session = sessionFactory.openSession()){
            Query<T> query = session.createQuery("from " + tClass.getSimpleName(), tClass);
            resultList = query.getResultList();
        }catch (HibernateException h){
            log.log(Level.SEVERE, "Hibernate exception.");
        }
        return resultList;
    }

    public static boolean removeEntity(BaseEntity entity){
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Transaction transaction = null;
        try(Session session = sessionFactory.openSession()){
            transaction = session.beginTransaction();
            session.remove(entity);
            transaction.commit();
            return true;
        }catch (IllegalArgumentException il) {
            log.log(Level.SEVERE, "The instatce is not an entity.");
            transaction.rollback();
        }catch (HibernateException he){
            log.log(Level.SEVERE,"Error login");
        }
        return false;
    }
    public static <T> boolean removeEntity(long id, Class<T> tClass){
        Optional optional = getEntityById(id,tClass);
        if (optional.isPresent()){
            boolean b = removeEntity((BaseEntity) optional.get());
        }
        return false;
    }
    public static <T> List<T> getListSortBy(String columnName, Class<T> tClass){
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        List<T> list = new ArrayList<>();
        try(Session session = sessionFactory.openSession()){
            String query = "from "+tClass.getSimpleName()+" k order by k."+columnName;
            Query<T> query1 = session.createQuery(query, tClass);
            list = query1.getResultList();
        }catch (IllegalStateException is){
            log.log(Level.SEVERE,"");
        }catch (HibernateException he){
            log.log(Level.SEVERE,"Hibernate error");
        }
        return list;
    }
    public static <T,M> List<T> getListFilterBy(String columnName, M mTyp, Class<T> tClass){
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        List<T> list = new ArrayList<>();
        try(Session session = sessionFactory.openSession()){
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<T> query = criteriaBuilder.createQuery(tClass);
            Root<T> root = query.from(tClass);
            query.select(root).where(criteriaBuilder.equal(root.get(columnName), mTyp));
            Query<T> studentQuery = session.createQuery(query);

            list = studentQuery.getResultList();
        }catch (NoResultException nr){
            log.log(Level.INFO,"Result not found.");
        }catch (HibernateException he){
            log.log(Level.SEVERE, "Hibernate exception just happend.");
        }
        return list;
    }
    public static <T,M> List<T> getListFilterBy(Predicate predicate, Class<T> tClass){
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        List<T> list = new ArrayList<>();
        try(Session session = sessionFactory.openSession()){
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<T> query = criteriaBuilder.createQuery(tClass);
            Root<T> root = query.from(tClass);
            query.select(root).where(predicate);
            Query<T> studentQuery = session.createQuery(query);

            list = studentQuery.getResultList();
        }catch (NoResultException nr){
            log.log(Level.INFO,"Result not found.");
        }catch (HibernateException he){
            log.log(Level.SEVERE, "Hibernate exception just happend.");
        }
        return list;
    }

    public static List<MechanicSQL> getMechanicFilterByAutoRepairShop(Long id){
        List<MechanicSQL> mechanicSQLList = new ArrayList<>();
        SessionFactory factory = HibernateUtil.getSessionFactory();
        try (Session session = factory.openSession()) {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<MechanicSQL> query = criteriaBuilder.createQuery(MechanicSQL.class);
            Root<AutoRepairShopSQL> root = query.from(AutoRepairShopSQL.class);

            Predicate p1 = criteriaBuilder.equal(root.get("id"), id);
            query.select(root.get("mechanicSQLSet")).where(p1);//kolejne predykaty po przecinku

            Query<MechanicSQL> mechanicSQLQuery = session.createQuery(query);
            mechanicSQLList = mechanicSQLQuery.getResultList();
        } catch (NoResultException nre) {
            log.log(Level.SEVERE, "No results."); // severe == error
        } catch (HibernateException he) {
            log.log(Level.SEVERE, "Error loading objects.");
        }
        return mechanicSQLList;
    }

    public static CarSQL getOldestCar(){
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        CarSQL carSQL = null;
        try(Session session = sessionFactory.openSession()){
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<CarSQL> query = criteriaBuilder.createQuery(CarSQL.class);
            Root<CarSQL> root = query.from(CarSQL.class);
            query.orderBy(criteriaBuilder.asc(root.get("productionDate")));
            Query<CarSQL> query1 = session.createQuery(query).setMaxResults(1);
            carSQL = query1.getSingleResult();
        }catch (NoResultException n){
            log.log(Level.SEVERE, "Sorry, no result.");
        }catch (HibernateException h){
            log.log(Level.SEVERE, "Hello, I'm HibernateException.");
        }
        return carSQL;
    }
    public static CarSQL getYoungestCar(){
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        CarSQL carSQL = null;
        try(Session session = sessionFactory.openSession()){
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<CarSQL> query = criteriaBuilder.createQuery(CarSQL.class);
            Root<CarSQL> root = query.from(CarSQL.class);
            query.orderBy(criteriaBuilder.desc(root.get("productionDate")));
            Query<CarSQL> query1 = session.createQuery(query).setMaxResults(1);
            carSQL = query1.getSingleResult();
        }catch (NoResultException n){
            log.log(Level.SEVERE, "Sorry, no result.");
        }catch (HibernateException h){
            log.log(Level.SEVERE, "Hello, I'm HibernateException.");
        }
        return carSQL;
    }
    public static CarSQL getCarMaxKm(){
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        CarSQL carSQL = null;
        try(Session session = sessionFactory.openSession()){
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<CarSQL> query = criteriaBuilder.createQuery(CarSQL.class);
            Root<CarSQL> root = query.from(CarSQL.class);
            query.orderBy(criteriaBuilder.desc(root.get("km")));
            Query<CarSQL> carQuery = session.createQuery(query).setMaxResults(1);
            carSQL = carQuery.getSingleResult();
        }catch (NoResultException n){
            log.log(Level.SEVERE, "Sorry, no result.");
        }catch (HibernateException h){
            log.log(Level.SEVERE, "Hello, I'm HibernateException.");
        }
        return carSQL;
    }
    public static CarSQL getCarMinKm(){
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        CarSQL carSQL = null;
        try(Session session = sessionFactory.openSession()){
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<CarSQL> query = criteriaBuilder.createQuery(CarSQL.class);
            Root<CarSQL> root = query.from(CarSQL.class);
            query.orderBy(criteriaBuilder.asc(root.get("km")));
            Query<CarSQL> carQuery = session.createQuery(query).setMaxResults(1);
            carSQL = carQuery.getSingleResult();
        }catch (NoResultException n){
            log.log(Level.SEVERE, "Sorry, no result.");
        }catch (HibernateException h){
            log.log(Level.SEVERE, "Hello, I'm HibernateException.");
        }
        return carSQL;
    }
    public static boolean addRelation_Car_Mechanic(long carId, long mechanicId){
        Optional<CarSQL> optionalCar = DAO.getEntityById(carId, CarSQL.class);
        Optional<MechanicSQL> optionalMechanic = DAO.getEntityById(mechanicId, MechanicSQL.class);
        Transaction transaction = null;
        if (optionalCar.isPresent()&&optionalMechanic.isPresent()){
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            try(Session session = sessionFactory.openSession()){
                MechanicSQL mechanicSQL = optionalMechanic.get();
                CarSQL carSQL = optionalCar.get();

                transaction = session.beginTransaction();
                carSQL.setMechanicSQL(mechanicSQL);
                session.saveOrUpdate(carSQL);
                transaction.commit();
                return true;
            }catch (IllegalStateException il){
                transaction.rollback();
                log.log(Level.SEVERE, "Transaction rollback.");
            }catch (HibernateException he){
                log.log(Level.SEVERE, "Hibernate exception.");
            }
        }
        return false;
    }
    public static boolean removeRelation_Car_Mechanic(long carId, long mechanicId){
        Optional<CarSQL> optionalCar = DAO.getEntityById(carId, CarSQL.class);
        Optional<MechanicSQL> optionalMechanic = DAO.getEntityById(mechanicId, MechanicSQL.class);
        Transaction transaction = null;
        if (optionalCar.isPresent()&&optionalMechanic.isPresent()){
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            try(Session session = sessionFactory.openSession()){
                CarSQL carSQL = optionalCar.get();
                MechanicSQL mechanicSQL = optionalMechanic.get();

                transaction = session.beginTransaction();
                if (Objects.deepEquals(carSQL.getMechanicSQL(), mechanicSQL)){
                    carSQL.setMechanicSQL(null);
                    session.saveOrUpdate(carSQL);
                }
                transaction.commit();
                return true;
            }catch (IllegalStateException il){
                transaction.rollback();
                log.log(Level.SEVERE, "Transaction rollback.");
            }catch (HibernateException he){
                log.log(Level.SEVERE, "Hibernate exception.");
            }
        }
        return false;
    }

    public static boolean addRelation_AutoRepairShop_Mechanic(long autoRepairShopId, long mechanicId){
        Optional<AutoRepairShopSQL> optionalAutoRepairShop = DAO.getEntityById(autoRepairShopId, AutoRepairShopSQL.class);
        Optional<MechanicSQL> optionalMechanic = DAO.getEntityById(mechanicId, MechanicSQL.class);
        Transaction transaction = null;
        if (optionalAutoRepairShop.isPresent()&&optionalMechanic.isPresent()){
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            try(Session session = sessionFactory.openSession()){
                MechanicSQL mechanicSQL = optionalMechanic.get();
                AutoRepairShopSQL autoRepairShopSQL = optionalAutoRepairShop.get();

                transaction = session.beginTransaction();
                mechanicSQL.setAutoRepairShopSQL(autoRepairShopSQL);
                session.saveOrUpdate(mechanicSQL);
                autoRepairShopSQL.getMechanicSQLSet().add(mechanicSQL);
                session.saveOrUpdate(autoRepairShopSQL);
                transaction.commit();

                return true;
            }catch (IllegalStateException il){
                transaction.rollback();
                log.log(Level.SEVERE, "Transaction rollback.");
            }catch (HibernateException he){
                log.log(Level.SEVERE, "Hibernate exception.");
            }
        }
        return false;
    }
    public static boolean removeRelation_AutoRepairShop_Mechanic(long autoRepairShopId, long mechanicId){
        Optional<AutoRepairShopSQL> optionalAutoRepairShop = DAO.getEntityById(autoRepairShopId, AutoRepairShopSQL.class);
        Optional<MechanicSQL> optionalMechanic = DAO.getEntityById(mechanicId, MechanicSQL.class);
        Transaction transaction = null;
        if (optionalAutoRepairShop.isPresent()&&optionalMechanic.isPresent()){
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            try(Session session = sessionFactory.openSession()){
                MechanicSQL mechanicSQL = optionalMechanic.get();
                AutoRepairShopSQL autoRepairShopSQL = optionalAutoRepairShop.get();

                transaction = session.beginTransaction();
                if (Objects.deepEquals(mechanicSQL.getAutoRepairShopSQL(), autoRepairShopSQL)){
                    autoRepairShopSQL.getMechanicSQLSet().remove(mechanicSQL);
                    session.saveOrUpdate(autoRepairShopSQL);
                    mechanicSQL.setAutoRepairShopSQL(null);
                    session.saveOrUpdate(mechanicSQL);
                }
                transaction.commit();
                return true;
            }catch (IllegalStateException il){
                transaction.rollback();
                log.log(Level.SEVERE, "Transaction rollback.");
            }catch (HibernateException he){
                log.log(Level.SEVERE, "Hibernate exception.");
            }
        }
        return false;
    }
    public static boolean setRepairOrder(RepairOrderSQL repairOrderSQL, MechanicSQL mechanicSQL,CarSQL carSQL, AutoRepairShopSQL autoRepairShopSQL){

    return saveOrUpdateEntity(repairOrderSQL);
    }


    public static List<RepairOrderSQL> getRepairOrderFilteredList(Optional<LocalDate> from, Optional<LocalDate> to, Boolean selectedStatus,
                                                                  CarSQL carSQL,AutoRepairShopSQL autoRepairShopSQL, MechanicSQL mechanicSQL) {
        List<RepairOrderSQL> repairOrderSQL = new ArrayList<>();
        List<Predicate> myPredicatesList = new ArrayList<>();
        SessionFactory factory = HibernateUtil.getSessionFactory();
        try (Session session = factory.openSession()) {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<RepairOrderSQL> query = criteriaBuilder.createQuery(RepairOrderSQL.class);
            Root<RepairOrderSQL> root = query.from(RepairOrderSQL.class);
            //from
            if (from.isPresent()){
                LocalDateTime fromLocalDateTime = from.get().atStartOfDay();
                Predicate fromPredicate = criteriaBuilder.greaterThanOrEqualTo(root.get("dateOfOrder"), fromLocalDateTime);
                myPredicatesList.add(fromPredicate);
            }
            //to
            if (to.isPresent()){
                LocalDateTime toLocalDateTime = to.get().atTime(23,59,59);
                Predicate toPredicate = criteriaBuilder.lessThanOrEqualTo(root.get("dateOfOrder"),toLocalDateTime);
                myPredicatesList.add(toPredicate);
            }
            if (selectedStatus!=null){
                Predicate toPredicate = criteriaBuilder.equal(root.get("repaired"), selectedStatus);
                myPredicatesList.add(toPredicate);
            }
            //car
            if (carSQL!=null){
                Join<RepairOrderSQL, CarSQL> carFromOrder = root.join("carSQL");
                Predicate carPredicate = criteriaBuilder.equal(carFromOrder.get("id"),carSQL.getId());
                myPredicatesList.add(carPredicate);
            }
            //autoRepairShop
            if (autoRepairShopSQL!=null){
                Join<RepairOrderSQL, AutoRepairShopSQL> autoRepairShopSQLFromOrder = root.join("autoRepairShopSQL");
                Predicate autoRepairShopPredicate = criteriaBuilder.equal(autoRepairShopSQLFromOrder.get("id"),autoRepairShopSQL.getId());
                myPredicatesList.add(autoRepairShopPredicate);
            }
            //mechanic
            if (mechanicSQL!=null){
                Join<RepairOrderSQL, MechanicSQL> mechanicFromOrder = root.join("mechanicSQL");
                Predicate mechanicPredicate = criteriaBuilder.equal(mechanicFromOrder.get("id"),mechanicSQL.getId());
                myPredicatesList.add(mechanicPredicate);
            }

            query.select(root).where(myPredicatesList.toArray(new Predicate[]{}));//kolejne predykaty po przecinku

            Query<RepairOrderSQL> repairOrderSQLQuery = session.createQuery(query);
            repairOrderSQL = repairOrderSQLQuery.getResultList();
        } catch (NoResultException nre) {
            log.log(Level.SEVERE, "No results."); // severe == error
        } catch (HibernateException he) {
            log.log(Level.SEVERE, "Error loading objects.");
        }
        return repairOrderSQL;
    }
}
