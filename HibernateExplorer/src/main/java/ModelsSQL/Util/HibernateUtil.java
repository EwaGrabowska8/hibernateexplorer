package ModelsSQL.Util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

/**
 * tworzenie połączenia do bazy danych
 * - załadowanie pliku konfiguracyjnego - i stworzenie fabryki obiektów w tym przypadku sesji
 * Zamiast tworzyć połączenie ręcznie, ono stworzy sie za nas
 */
public class HibernateUtil {
    //tworzenie sesji - statyczna fabryka sesji
    private static SessionFactory sessionFactory;

    //blok static - blok statycznej inicjalizacji - wywoła się po pierwzym użyciu klasy w projekcie
    //w takim bloku - bardziej pewne, że wykona się zawsze, albo prywatny konstruktor - jeżeli nasza aplikacji korzysta z bazy danych, powinna być jedna sesja
    static{
        // Tworzymy sobie obiekt, który pobiera konfigurację z pliku hibernate cfg xml
        StandardServiceRegistry standardServiceRegistry =
                new StandardServiceRegistryBuilder()
                        .configure("hibernate.cfg.xml").build();

        // metadata to informacje dotyczące plików. z danych załadowanych wcześniej
        // towrzymy sobie obiekt metadata.
        Metadata metadata = new MetadataSources(standardServiceRegistry)
                .getMetadataBuilder().build();

        // stworzenie sesji z danych zawartych w pliku hibernate cfg xml
        sessionFactory = metadata.getSessionFactoryBuilder().build();
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
