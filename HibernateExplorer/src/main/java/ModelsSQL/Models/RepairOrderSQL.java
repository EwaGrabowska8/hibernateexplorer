package ModelsSQL.Models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RepairOrderSQL extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "YOUR_ENTITY_SEQ_RepairOrder")
    @SequenceGenerator(name = "YOUR_ENTITY_SEQ_RepairOrder", sequenceName = "YOUR_ENTITY_SEQ_RepairOrder", allocationSize = 1)
    private Long id;
    private String description;
    private boolean repaired;
    private LocalDateTime dateOfOrder;
    private LocalDateTime dateOfModify;

    @ManyToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private CarSQL carSQL;

    @ManyToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private AutoRepairShopSQL autoRepairShopSQL;

    @ManyToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private MechanicSQL mechanicSQL;

    @PrePersist
    protected void onPersist(){
        this.dateOfOrder = LocalDateTime.now();
        this.dateOfModify = LocalDateTime.now();
    }

    @PreUpdate
    protected void onUpdate() {
        this.dateOfModify = LocalDateTime.now();
    }

    public RepairOrderSQL(String description, boolean repaired) {
        this.description = description;
        this.repaired = repaired;
    }

    public RepairOrderSQL(String description, boolean repaired, CarSQL carSQL, AutoRepairShopSQL autoRepairShopSQL, MechanicSQL mechanicSQL) {
        this.description = description;
        this.repaired = repaired;
        this.carSQL = carSQL;
        this.autoRepairShopSQL = autoRepairShopSQL;
        this.mechanicSQL = mechanicSQL;
    }
}

