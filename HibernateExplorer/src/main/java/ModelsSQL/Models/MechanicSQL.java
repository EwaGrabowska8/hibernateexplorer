package ModelsSQL.Models;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MechanicSQL extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "YOUR_ENTITY_SEQ_Mechanic")
    @SequenceGenerator(name = "YOUR_ENTITY_SEQ_Mechanic", sequenceName = "YOUR_ENTITY_SEQ_Mechanic", allocationSize = 1)
    private Long id;

    private String name;
    private String surname;
    private long pesel;
    private boolean isArchival;

    @ManyToOne(cascade = CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private AutoRepairShopSQL autoRepairShopSQL;

    @OneToMany(mappedBy = "mechanicSQL", fetch = FetchType.EAGER)
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<CarSQL> carSQLSet = new HashSet<>();

    @OneToMany(mappedBy = "mechanicSQL", fetch = FetchType.EAGER)
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<RepairOrderSQL> repairOrderSQLSet = new HashSet<>();

    public MechanicSQL(String name, String surname, long pesel) {
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
        this.isArchival = false;
    }
}
