package ModelsSQL.Models;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AutoRepairShopSQL extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "YOUR_ENTITY_SEQ_AutoRepairShop")
    @SequenceGenerator(name = "YOUR_ENTITY_SEQ_AutoRepairShop", sequenceName = "YOUR_ENTITY_SEQ_AutoRepairShop", allocationSize = 1)
    private Long id;
    private String name;
    private String adress;
    private long NIP;
    private boolean isArchival;

    @OneToMany(mappedBy = "autoRepairShopSQL", fetch = FetchType.EAGER)
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<MechanicSQL> mechanicSQLSet = new HashSet<>();

    @OneToMany(mappedBy = "autoRepairShopSQL", fetch = FetchType.EAGER)
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<RepairOrderSQL> repairOrderSQLSet = new HashSet<>();

    public AutoRepairShopSQL(String name, String adress, long NIP) {
        this.name = name;
        this.adress = adress;
        this.NIP = NIP;
        this.isArchival = false;
    }
}