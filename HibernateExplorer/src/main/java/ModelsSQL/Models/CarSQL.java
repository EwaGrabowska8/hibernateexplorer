package ModelsSQL.Models;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CarSQL extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "YOUR_ENTITY_SEQ_Car")
    @SequenceGenerator(name = "YOUR_ENTITY_SEQ_Car", sequenceName = "YOUR_ENTITY_SEQ_Car", allocationSize = 1)
    private Long id;

    private String registrationNumber;
    private String brand;
    private String model;
    @Enumerated(EnumType.STRING)
    private CarBodyType carBodyType;
    private LocalDate productionDate;
    private String color;
    private long km;
    private LocalDateTime modifiedDate; // – pole to ma być automatycznie ustawiane modyfikacji tej encji
    private boolean isArchival;

    @ManyToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private MechanicSQL mechanicSQL;

    @OneToMany(mappedBy = "carSQL",fetch = FetchType.EAGER)
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<RepairOrderSQL> repairOrderSQLSet = new HashSet<>();

    @PreUpdate
    protected void onUpdate() {
        this.modifiedDate = LocalDateTime.now();
    }

    public CarSQL(String registrationNumber, String brand, String model, CarBodyType carBodyType, LocalDate productionDate, String color, long km) {
        this.registrationNumber = registrationNumber;
        this.brand = brand;
        this.model = model;
        this.carBodyType = carBodyType;
        this.productionDate = productionDate;
        this.color = color;
        this.km = km;
        this.modifiedDate = LocalDateTime.now();
        this.isArchival = false;
    }
}

