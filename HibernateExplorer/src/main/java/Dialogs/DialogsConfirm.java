package Dialogs;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;
import java.util.ResourceBundle;

public class DialogsConfirm {
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("bundles.messages");

    public static <T> Optional<ButtonType> archivalStatusDialog(Class<T> tClass) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(resourceBundle.getString("archiveConfirmTitle"));
        alert.setHeaderText(resourceBundle.getString("archiveConfirmHead" + tClass.getSimpleName().substring(0, 1)));
        Optional<ButtonType> result = alert.showAndWait();
        return result;
    }

}
