package Dialogs;

import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ResourceBundle;

public class DialogsErrorAplication {
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("bundles.messages");

    public static void daoExceptionDialog(Exception e) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(resourceBundle.getString("dataBase answer"));
        alert.setHeaderText(resourceBundle.getString("Exception Dialog"));

        Label label = new Label(resourceBundle.getString("The exception stacktrace was:"));

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String exceptionText = sw.toString();

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(textArea, 0, 1);

        alert.getDialogPane().setExpandableContent(expContent);
        alert.showAndWait();
    }

    public static void LongParseExceptionDialog(String valueToParse) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(resourceBundle.getString("longParseExceptionTittle"));
        alert.setHeaderText(valueToParse + resourceBundle.getString("enteredValue"));
        alert.setContentText(resourceBundle.getString("enter correct data"));

        alert.showAndWait();
    }

    public static void lenghtExceptionDialog(String inputValue, int expectedLenght) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(resourceBundle.getString("lenghtExceptionTittle"));
        alert.setHeaderText(inputValue + resourceBundle.getString("inputValueLenght") + " " + expectedLenght + ".");
        alert.setContentText(resourceBundle.getString("enter correct data"));

        alert.showAndWait();
    }


}
