package Dialogs;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;
import java.util.ResourceBundle;

public class DialogsMenuBar {
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("bundles.messages");

    public static Optional<ButtonType> confirmationExitDialog(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(resourceBundle.getString("confirmTitle"));
        alert.setHeaderText(resourceBundle.getString("confirmHead"));
        Optional<ButtonType> result = alert.showAndWait();
        return result;
    }

}
