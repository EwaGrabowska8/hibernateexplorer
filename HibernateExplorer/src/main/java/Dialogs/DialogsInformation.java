package Dialogs;

import javafx.scene.control.Alert;

import java.util.ResourceBundle;

public class DialogsInformation {
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("bundles.messages");

    public static void addSukcess() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(resourceBundle.getString("dataBase answer"));
        alert.setHeaderText(resourceBundle.getString("addSukcessText"));

        alert.showAndWait();
    }


}
