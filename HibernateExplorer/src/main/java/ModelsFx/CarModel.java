package ModelsFx;

import ModelsSQL.DAO.DAO;
import ModelsSQL.Models.CarSQL;
import ModelsSQL.Models.CarBodyType;
import ModelsSQL.Models.MechanicSQL;
import ModelsSQL.Models.RepairOrderSQL;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.*;

@NoArgsConstructor
@AllArgsConstructor
public class CarModel {

    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("bundles.messages");

    private ObservableList<CarFx> observableList = FXCollections.observableArrayList();
    private ObservableList<CarFx> observableListArchive = FXCollections.observableArrayList();
    private static ObjectProperty<CarFx> objectProperty = new SimpleObjectProperty<>();
    private static ObjectProperty<CarFx> objectPropertyArchive = new SimpleObjectProperty<>();

    private List<CarSQL> carSQLList = new ArrayList<>();
    private TreeItem<String> root = new TreeItem<>();

    public TreeItem<String> getRoot() {
        return root;
    }

    public void setRoot(TreeItem<String> root) {
        this.root = root;
    }

    public ObservableList<CarFx> getObservableList() {
        return observableList;
    }

    public void setObservableList(ObservableList<CarFx> observableList) {
        this.observableList = observableList;
    }

    public CarFx getObjectProperty() {
        return objectProperty.get();
    }

    public ObjectProperty<CarFx> objectPropertyProperty() {
        return objectProperty;
    }

    public void setObjectProperty(CarFx objectProperty) {
        this.objectProperty.set(objectProperty);
    }

    public ObservableList<CarFx> getObservableListArchive() {
        return observableListArchive;
    }

    public void setObservableListArchive(ObservableList<CarFx> observableListArchive) {
        this.observableListArchive = observableListArchive;
    }

    public static CarFx getObjectPropertyArchive() {
        return objectPropertyArchive.get();
    }

    public static ObjectProperty<CarFx> objectPropertyArchiveProperty() {
        return objectPropertyArchive;
    }

    public static void setObjectPropertyArchive(CarFx objectPropertyArchive) {
        CarModel.objectPropertyArchive.set(objectPropertyArchive);
    }

    public void addCar(String registrationNumber, String brand, String model, CarBodyType carBodyType, LocalDate productionDate, String color, long km) {
        CarSQL carSQL = new CarSQL(registrationNumber, brand, model, carBodyType, productionDate, color, km);
        DAO.saveOrUpdateEntity(carSQL);
    }

    public void initList() {
        this.carSQLList = DAO.getAll(CarSQL.class);
        observableList.clear();
        this.carSQLList.forEach(car -> {
            if (!car.isArchival()) {
                observableList.add(convert_car_to_carFx(car));
            } else {
                observableListArchive.add(convert_car_to_carFx(car));
            }
        });
    }

    public static CarSQL convert_carFx_to_car(CarFx carFx) {
        CarSQL carSQL = new CarSQL();
        if (carFx.getMechanic() != null) {
            MechanicSQL mechanicSQL = MechanicModel.convert_mechanicFx_to_mechanic(carFx.getMechanic());
            carSQL.setMechanicSQL(mechanicSQL);
        }
        carSQL.setRegistrationNumber(carFx.getRegistrationNumber());
        carSQL.setBrand(carFx.getBrand());
        carSQL.setCarBodyType(carFx.getCarBodyType());
        carSQL.setColor(carFx.getColor());
        carSQL.setId(carFx.getId());
        carSQL.setKm(carFx.getKm());
        carSQL.setModel(carFx.getModel());
        carSQL.setModifiedDate(carFx.getModifiedDate());
        carSQL.setProductionDate(carFx.getProductionDate());
        return carSQL;
    }

    public static CarFx convert_car_to_carFx(CarSQL carSQL) {
        CarFx carFx = new CarFx();
        if (carSQL.getMechanicSQL() != null) {
            MechanicFx mechanicFx = MechanicModel.convert_mechanic_to_mechanicFx(carSQL.getMechanicSQL());
            carFx.setMechanic(mechanicFx);
        }
        carFx.setRegistrationNumber(carSQL.getRegistrationNumber());
        carFx.setBrand(carSQL.getBrand());
        carFx.setCarBodyType(carSQL.getCarBodyType());
        carFx.setColor(carSQL.getColor());
        carFx.setId(carSQL.getId());
        carFx.setKm(carSQL.getKm());
        carFx.setModel(carSQL.getModel());
        carFx.setModifiedDate(carSQL.getModifiedDate());
        carFx.setProductionDate(carSQL.getProductionDate());
        return carFx;
    }

    public void initRoot() {
        this.carSQLList.forEach(car -> {
            if (!car.isArchival()) {
                setItemCar(car);
            }
        });

    }

    public void setItemCar(CarSQL car) {
        String carToString = car.getRegistrationNumber() + ", " + car.getBrand() + " " + car.getModel();
        TreeItem<String> carItem = new TreeItem<>(carToString);
        root.getChildren().add(carItem);

        TreeItem<String> allRepairOrdersItem = new TreeItem<>(resourceBundle.getString("ordersTB"));
        carItem.getChildren().add(allRepairOrdersItem);
        Set<RepairOrderSQL> repairOrderSQLSet = car.getRepairOrderSQLSet();
        repairOrderSQLSet.forEach(repairOrder -> {
            String repairOrderToString = repairOrder.getDateOfOrder() + ", " + repairOrder.getAutoRepairShopSQL() + ", " + repairOrder.getCarSQL();
            TreeItem<String> singleRepairOrder = new TreeItem<>(repairOrderToString);
            allRepairOrdersItem.getChildren().add(singleRepairOrder);

        });
    }

    public void updateCar(String registrationNumber, String brand, String model, CarBodyType carBodyType, LocalDate productionDate, String color, long km, long carToUpdateid) {
        CarSQL carSQL = this.findCarToUpdateById(carToUpdateid);
        carSQL.setRegistrationNumber(registrationNumber);
        carSQL.setBrand(brand);
        carSQL.setModel(model);
        carSQL.setCarBodyType(carBodyType);
        carSQL.setProductionDate(productionDate);
        carSQL.setColor(color);
        carSQL.setKm(km);
        DAO.saveOrUpdateEntity(carSQL);
    }

    private CarSQL findCarToUpdateById(long carToUpdateid) {
        Optional<CarSQL> entityById = DAO.getEntityById(carToUpdateid, CarSQL.class);
        if (entityById.isPresent()) {
            return entityById.get();
        }
        return null;
    }

    public void setIsArchiveAndRemoveRelation() {
        long id = objectProperty.get().getId();
        CarSQL carSQLToUpdate = findCarToUpdateById(id);
        this.setIsArchive(carSQLToUpdate);
        this.removeAllRelation(carSQLToUpdate);
    }

    private void removeAllRelation(CarSQL carSQLToUpdate) {
        MechanicSQL mechanicSQL = carSQLToUpdate.getMechanicSQL();
        if (mechanicSQL != null) {
            long idCarSQLToUpdate = carSQLToUpdate.getId();
            long idMechanic = mechanicSQL.getId();
            DAO.removeRelation_Car_Mechanic(idCarSQLToUpdate, idMechanic);
        }

    }

    public void setIsArchive(CarSQL carSQLToUpdate) {
        carSQLToUpdate.setArchival(true);
        DAO.saveOrUpdateEntity(carSQLToUpdate);
    }

}
