package ModelsFx;

import FxmlControllers.ViewBuilder;
import ModelsSQL.DAO.DAO;
import ModelsSQL.Models.*;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Toggle;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

public class RepairOrderModel {
    private ObservableList<RepairOrderFx> observableList = FXCollections.observableArrayList();
    private static ObjectProperty<RepairOrderFx> objectProperty = new SimpleObjectProperty<>();

    public ObservableList<RepairOrderFx> getObservableList() {
        return observableList;
    }

    public void setObservableList(ObservableList<RepairOrderFx> observableList) {
        this.observableList = observableList;
    }

    public RepairOrderFx getObjectProperty() {
        return objectProperty.get();
    }

    public ObjectProperty<RepairOrderFx> objectPropertyProperty() {
        return objectProperty;
    }

    public void setObjectProperty(RepairOrderFx objectProperty) {
        this.objectProperty.set(objectProperty);
    }

    public boolean addRepairOrder(String description, CarFx carFx, AutoRepairShopFx autoRepairShopFx, MechanicFx mechanicFx) {
        CarSQL carSQL = CarModel.convert_carFx_to_car(carFx);
        AutoRepairShopSQL autoRepairShopSQL = AutoRepairShopModel.convert_autoRepairShopFx_to_autoRepairShop(autoRepairShopFx);
        MechanicSQL mechanicSQL = MechanicModel.convert_mechanicFx_to_mechanic(mechanicFx);
        RepairOrderSQL repairOrderSQL = new RepairOrderSQL(description,false,carSQL,autoRepairShopSQL,mechanicSQL);
        System.out.println(repairOrderSQL);
        return DAO.saveOrUpdateEntity(repairOrderSQL);
    }

    public void initList() {
        List<RepairOrderSQL> list = DAO.getAll(RepairOrderSQL.class);
        observableList.clear();
        list.forEach(repairOrder -> observableList.add(convert_repairOrder_to_repairOrderFx(repairOrder)));
    }

    public static RepairOrderSQL convert_repairOrderFx_to_repairOrder(RepairOrderFx repairOrderFx) {
        RepairOrderSQL repairOrderSQL = new RepairOrderSQL();
        repairOrderSQL.setId(repairOrderFx.getId());
        repairOrderSQL.setDescription(repairOrderFx.getDescription());
        repairOrderSQL.setRepaired(repairOrderFx.getRepaired());
        repairOrderSQL.setAutoRepairShopSQL
                (AutoRepairShopModel.convert_autoRepairShopFx_to_autoRepairShop(repairOrderFx.getAutoRepairShopFx()));
        repairOrderSQL.setCarSQL(CarModel.convert_carFx_to_car(repairOrderFx.getCarFx()));
        repairOrderSQL.setMechanicSQL(MechanicModel.convert_mechanicFx_to_mechanic(repairOrderFx.getMechanicFx()));
        repairOrderSQL.setDateOfModify(repairOrderFx.getDateOdModify());
        repairOrderSQL.setDateOfOrder(repairOrderFx.getDateOfOrde());
        return repairOrderSQL;
    }

    public static RepairOrderFx convert_repairOrder_to_repairOrderFx(RepairOrderSQL repairOrderSQL) {
        RepairOrderFx repairOrderFx = new RepairOrderFx();
        repairOrderFx.setId(repairOrderSQL.getId());
        repairOrderFx.setDescription(repairOrderSQL.getDescription());
        repairOrderFx.setRepaired(repairOrderSQL.isRepaired());

        AutoRepairShopFx autoRepairShopFx = null;
        if (repairOrderSQL.getAutoRepairShopSQL() != null) {
            autoRepairShopFx = AutoRepairShopModel.convert_autoRepairShop_to_autoRepairShopFx(repairOrderSQL.getAutoRepairShopSQL());
        }
        repairOrderFx.setAutoRepairShopFx(autoRepairShopFx);

        CarFx carFx = null;
        if (repairOrderSQL.getCarSQL() != null) {
            carFx = CarModel.convert_car_to_carFx(repairOrderSQL.getCarSQL());
        }
        repairOrderFx.setCarFx(carFx);

        MechanicFx mechanicFx = null;
        if (repairOrderSQL.getMechanicSQL() != null) {
            mechanicFx = MechanicModel.convert_mechanic_to_mechanicFx(repairOrderSQL.getMechanicSQL());
        }
        repairOrderFx.setMechanicFx(mechanicFx);

        repairOrderFx.setDateOdModify(repairOrderSQL.getDateOfModify());
        repairOrderFx.setDateOfOrde(repairOrderSQL.getDateOfOrder());
        return repairOrderFx;
    }

    public void updateRepairOrder(String description, CarFx carFx, AutoRepairShopFx autoRepairShopFx, MechanicFx mechanicFx, long id) {
        Optional<RepairOrderSQL> repairOrderSQLOptional = DAO.getEntityById(id, RepairOrderSQL.class);
        RepairOrderSQL repairOrderToUpdate = repairOrderSQLOptional.get();

        repairOrderToUpdate.setDescription(description);

        CarSQL carSQL = null;
        if (carFx != null) {
            carSQL = CarModel.convert_carFx_to_car(carFx);
        }
        repairOrderToUpdate.setCarSQL(carSQL);

        AutoRepairShopSQL autoRepairShopSQL = null;
        if (autoRepairShopFx != null) {
            autoRepairShopSQL = AutoRepairShopModel.convert_autoRepairShopFx_to_autoRepairShop(autoRepairShopFx);
        }
        repairOrderToUpdate.setAutoRepairShopSQL(autoRepairShopSQL);

        MechanicSQL mechanicSQL = null;
        if (mechanicFx != null) {
            mechanicSQL = MechanicModel.convert_mechanicFx_to_mechanic(mechanicFx);
        }
        repairOrderToUpdate.setDateOfModify(LocalDateTime.now());
        repairOrderToUpdate.setMechanicSQL(mechanicSQL);

        DAO.saveOrUpdateEntity(repairOrderToUpdate);
    }

    public RepairOrderSQL foundAutoRepairOrderById(long id) {
        Optional<RepairOrderSQL> entityById = DAO.getEntityById(id, RepairOrderSQL.class);
        if (entityById.isPresent()) {
            return entityById.get();
        }
        return null;
    }

    public void filterOvservableListByPredicats(Optional<LocalDate> from, Optional<LocalDate> to, Boolean selectedStatus,
                                                Optional<CarFx> optionalCarFx, Optional<AutoRepairShopFx> autoRepairShopFx, Optional<MechanicFx> mechanicFx) {
        CarSQL carSQL = null;
        if (optionalCarFx.isPresent()){
            carSQL = CarModel.convert_carFx_to_car(optionalCarFx.get());
        }
        AutoRepairShopSQL autoRepairShopSQL = null;
        if (autoRepairShopFx.isPresent()){
            autoRepairShopSQL = AutoRepairShopModel.convert_autoRepairShopFx_to_autoRepairShop(autoRepairShopFx.get());
        }
        MechanicSQL mechanicSQL = null;
        if(mechanicFx.isPresent()){
            mechanicSQL = MechanicModel.convert_mechanicFx_to_mechanic(mechanicFx.get());
        }
        List<RepairOrderSQL> filteredList = DAO.getRepairOrderFilteredList(from, to, selectedStatus,
                carSQL, autoRepairShopSQL, mechanicSQL);
        List<RepairOrderFx> repairOrderFxList = filteredList.stream()
                .map(RepairOrderModel::convert_repairOrder_to_repairOrderFx)
                .collect(Collectors.toList());
        this.observableList.setAll(repairOrderFxList);
        System.out.println(repairOrderFxList.size());
    }


    public void editOrderStatus(RepairOrderFx item) {
        ViewBuilder.setModalWindowEditStatus(item);
    }
}
