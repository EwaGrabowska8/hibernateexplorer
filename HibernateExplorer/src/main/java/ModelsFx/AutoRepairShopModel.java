package ModelsFx;

import ModelsSQL.DAO.DAO;
import ModelsSQL.Models.AutoRepairShopSQL;
import ModelsSQL.Models.MechanicSQL;
import ModelsSQL.Models.RepairOrderSQL;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.*;

@NoArgsConstructor
@AllArgsConstructor
public class AutoRepairShopModel {

    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("bundles.messages");

    private ObservableList<AutoRepairShopFx> observableList = FXCollections.observableArrayList();
    private ObservableList<AutoRepairShopFx> observableListArchive = FXCollections.observableArrayList();
    private static ObjectProperty<AutoRepairShopFx> objectProperty = new SimpleObjectProperty<>();
    private static ObjectProperty<AutoRepairShopFx> objectPropertyArchive = new SimpleObjectProperty<>();

    private List<AutoRepairShopSQL> autoRepairShopSQLArrayList = new ArrayList<>();
    private TreeItem<String> root = new TreeItem<>();

    public ObservableList<AutoRepairShopFx> getObservableList() {
        return observableList;
    }

    public void setObservableList(ObservableList<AutoRepairShopFx> observableList) {
        this.observableList = observableList;
    }

    public ObservableList<AutoRepairShopFx> getObservableListArchive() {
        return observableListArchive;
    }

    public void setObservableListArchive(ObservableList<AutoRepairShopFx> observableListArchive) {
        this.observableListArchive = observableListArchive;
    }

    public static AutoRepairShopFx getObjectPropertyArchive() {
        return objectPropertyArchive.get();
    }

    public static ObjectProperty<AutoRepairShopFx> objectPropertyArchiveProperty() {
        return objectPropertyArchive;
    }

    public static void setObjectPropertyArchive(AutoRepairShopFx objectPropertyArchive) {
        AutoRepairShopModel.objectPropertyArchive.set(objectPropertyArchive);
    }

    public AutoRepairShopFx getObjectProperty() {
        return objectProperty.get();
    }

    public ObjectProperty<AutoRepairShopFx> objectPropertyProperty() {
        return objectProperty;
    }

    public void setObjectProperty(AutoRepairShopFx objectProperty) {
        this.objectProperty.set(objectProperty);
    }

    public TreeItem<String> getRoot() {
        return root;
    }

    public void setRoot(TreeItem<String> root) {
        this.root = root;
    }

    public void initList() {
        observableList.clear();
        autoRepairShopSQLArrayList = DAO.getAll(AutoRepairShopSQL.class);
        autoRepairShopSQLArrayList.forEach(autoRepairShop -> {
            if (!autoRepairShop.isArchival()) {
                observableList.add(convert_autoRepairShop_to_autoRepairShopFx(autoRepairShop));
            } else {
                observableListArchive.add(convert_autoRepairShop_to_autoRepairShopFx(autoRepairShop));
            }
        });
    }

    public void initRoot() {
        autoRepairShopSQLArrayList.forEach(autoRepairShop -> {
            if (!autoRepairShop.isArchival()) {
                setItemsAutoRepaisShop(autoRepairShop);
            }
        });
    }

    public void setItemsAutoRepaisShop(AutoRepairShopSQL autoRepairShop) {
        String autoRepairShopToString = autoRepairShop.getName() + ", NIP: " + autoRepairShop.getNIP();
        TreeItem<String> autoRepairShopItem = new TreeItem<>(autoRepairShopToString);
        root.getChildren().add(autoRepairShopItem);

        TreeItem<String> allMechanicsItem = new TreeItem<>(resourceBundle.getString("mechanicsTB"));
        autoRepairShopItem.getChildren().add(allMechanicsItem);
        Set<MechanicSQL> mechanicSQLSet = autoRepairShop.getMechanicSQLSet();
        mechanicSQLSet.forEach(mechanic -> {
            if (!mechanic.isArchival()) {
                setItemMechanic(allMechanicsItem, mechanic);
            }
        });

        TreeItem<String> allRepairOrdersItem = new TreeItem<>(resourceBundle.getString("ordersTB"));
        autoRepairShopItem.getChildren().add(allRepairOrdersItem);
        Set<RepairOrderSQL> repairOrderSQLSet = autoRepairShop.getRepairOrderSQLSet();
        repairOrderSQLSet.forEach(repairOrder -> {
            String repairOrderToString = repairOrder.getId() + ", " + repairOrder.getCarSQL().getBrand() + " " + repairOrder.getCarSQL().getModel();
            TreeItem<String> repairOrderItem = new TreeItem<>(repairOrderToString);
            allRepairOrdersItem.getChildren().add(repairOrderItem);
        });
    }

    public void setItemMechanic(TreeItem<String> allMechanicsItem, MechanicSQL mechanic) {
        String mechanicToString = mechanic.getSurname() + " " + mechanic.getName() + ", PESEL: " + mechanic.getPesel();
        TreeItem<String> mechanicItem = new TreeItem<>(mechanicToString);
        allMechanicsItem.getChildren().add(mechanicItem);
    }

    public void addAutoRepairShop(String name, String adress, long NIP) {
        AutoRepairShopSQL autoRepairShopSQL = new AutoRepairShopSQL(name, adress, NIP);
        DAO.saveOrUpdateEntity(autoRepairShopSQL);
        this.initList();
    }

    public static AutoRepairShopFx convert_autoRepairShop_to_autoRepairShopFx(AutoRepairShopSQL autoRepairShopSQL) {
        AutoRepairShopFx autoRepairShopFx = new AutoRepairShopFx();
        autoRepairShopFx.setAdress(autoRepairShopSQL.getAdress());
        autoRepairShopFx.setName(autoRepairShopSQL.getName());
        autoRepairShopFx.setId(autoRepairShopSQL.getId());
        autoRepairShopFx.setNIP(autoRepairShopSQL.getNIP());
        autoRepairShopFx.setIsArchive(autoRepairShopSQL.isArchival());
        return autoRepairShopFx;
    }

    public static AutoRepairShopSQL convert_autoRepairShopFx_to_autoRepairShop(AutoRepairShopFx autoRepairShopFx) {
        AutoRepairShopSQL autoRepairShopSQL = new AutoRepairShopSQL();
        autoRepairShopSQL.setId(autoRepairShopFx.getId());
        autoRepairShopSQL.setName(autoRepairShopFx.getName());
        autoRepairShopSQL.setAdress(autoRepairShopFx.getAdress());
        autoRepairShopSQL.setNIP(autoRepairShopFx.getNIP());
        autoRepairShopSQL.setArchival(autoRepairShopFx.isIsArchive());
        return autoRepairShopSQL;
    }

    public void updateAutoRepairShop(String name, String adres, long NIP, long id) {
        AutoRepairShopSQL autoRepairShopSQLToUpdate = findAutoRepairShopById(id);
        autoRepairShopSQLToUpdate.setName(name);
        autoRepairShopSQLToUpdate.setAdress(adres);
        autoRepairShopSQLToUpdate.setNIP(NIP);
        DAO.saveOrUpdateEntity(autoRepairShopSQLToUpdate);
    }

    public AutoRepairShopSQL findAutoRepairShopById(long id) {
        Optional<AutoRepairShopSQL> entityById = DAO.getEntityById(id, AutoRepairShopSQL.class);
        if (entityById.isPresent()) {
            return entityById.get();
        }
        return null;
    }

    public void setIsArchiveAndRemoveRelation() {
        long id = objectProperty.get().getId();
        AutoRepairShopSQL autoRepairShopToUpdate = findAutoRepairShopById(id);
        this.setIsArchive(autoRepairShopToUpdate);
        this.removeRelation(autoRepairShopToUpdate);
    }

    public void removeRelation(AutoRepairShopSQL autoRepairShopToUpdate) {
        long idAutoRepairShop = autoRepairShopToUpdate.getId();
        Set<MechanicSQL> mechanicSQLSet = autoRepairShopToUpdate.getMechanicSQLSet();
        mechanicSQLSet.forEach(mechanicSQL -> {
            long idMechanic = mechanicSQL.getId();
            DAO.removeRelation_AutoRepairShop_Mechanic(idAutoRepairShop, idMechanic);
        });
    }

    public void setIsArchive(AutoRepairShopSQL autoRepairShopToUpdate) {
        autoRepairShopToUpdate.setArchival(true);
        DAO.saveOrUpdateEntity(autoRepairShopToUpdate);
    }
}
