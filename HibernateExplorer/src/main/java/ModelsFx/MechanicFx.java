package ModelsFx;

import javafx.beans.property.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class MechanicFx {
    private LongProperty id = new SimpleLongProperty();
    private StringProperty name = new SimpleStringProperty();
    private StringProperty surname = new SimpleStringProperty();
    private LongProperty pesel = new SimpleLongProperty();
    private ObjectProperty<AutoRepairShopFx> autoRepairShopFx = new SimpleObjectProperty<>();
    private BooleanProperty isArchive = new SimpleBooleanProperty();

    public long getId() {
        return id.get();
    }

    public LongProperty idProperty() {
        return id;
    }

    public void setId(long id) {
        this.id.set(id);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getSurname() {
        return surname.get();
    }

    public StringProperty surnameProperty() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname.set(surname);
    }

    public AutoRepairShopFx getAutoRepairShopFx() {
        return autoRepairShopFx.get();
    }

    public long getPesel() {
        return pesel.get();
    }

    public LongProperty peselProperty() {
        return pesel;
    }

    public void setPesel(long pesel) {
        this.pesel.set(pesel);
    }

    public ObjectProperty<AutoRepairShopFx> autoRepairShopFxProperty() {
        return autoRepairShopFx;
    }

    public void setAutoRepairShopFx(AutoRepairShopFx autoRepairShopFx) {
        this.autoRepairShopFx.set(autoRepairShopFx);
    }

    public boolean isIsArchive() {
        return isArchive.get();
    }

    public BooleanProperty isArchiveProperty() {
        return isArchive;
    }

    public void setIsArchive(boolean isArchive) {
        this.isArchive.set(isArchive);
    }

    @Override
    public String toString() {
        String toStringValue = this.surname.getValue() + " " + this.name.getValue()+", \nPESEL: "+this.pesel.getValue();
        return toStringValue;
    }
}
