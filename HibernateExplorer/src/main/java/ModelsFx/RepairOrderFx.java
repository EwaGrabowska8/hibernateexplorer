package ModelsFx;

import javafx.beans.property.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
public class RepairOrderFx {

    private LongProperty id = new SimpleLongProperty();
    private StringProperty description = new SimpleStringProperty();
    private BooleanProperty repaired = new SimpleBooleanProperty();
    private ObjectProperty<CarFx> carFx = new SimpleObjectProperty<>();
    private ObjectProperty<AutoRepairShopFx> autoRepairShopFx = new SimpleObjectProperty<>();
    private ObjectProperty<MechanicFx> mechanicFx = new SimpleObjectProperty<>();
    private ObjectProperty<LocalDateTime> dateOfOrde = new SimpleObjectProperty<>();
    private ObjectProperty<LocalDateTime> dateOdModify = new SimpleObjectProperty<>();
    private BooleanProperty tranferredToMechanic = new SimpleBooleanProperty();

    public boolean isTranferredToMechanic() {
        return tranferredToMechanic.get();
    }

    public BooleanProperty tranferredToMechanicProperty() {
        setTranferredToMechanic(setTransfer());
        return tranferredToMechanic;
    }

    private boolean setTransfer() {
        MechanicFx mechanic = mechanicFx.get();
        CarFx car = carFx.get();
        if (mechanic!=null&&car!=null){
            if(car.getMechanic()!=null){
                return true;
            }
        }
        return false;
    }

    public void setTranferredToMechanic(boolean tranferredToMechanic) {

        this.tranferredToMechanic.set(tranferredToMechanic);
    }

    public long getId() {
        return id.get();
    }

    public LongProperty idProperty() {
        return id;
    }

    public void setId(long id) {
        this.id.set(id);
    }

    public String getDescription() {
        return description.get();
    }

    public StringProperty descriptionProperty() {
        return description;
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public boolean getRepaired() {
        return repaired.get();
    }

    public BooleanProperty repairedProperty() {
        return repaired;
    }

    public void setRepaired(boolean repaired) {
        this.repaired.set(repaired);
    }

    public CarFx getCarFx() {
        return carFx.get();
    }

    public ObjectProperty<CarFx> carFxProperty() {
        return carFx;
    }

    public void setCarFx(CarFx carFx) {
        this.carFx.set(carFx);
    }

    public AutoRepairShopFx getAutoRepairShopFx() {
        return autoRepairShopFx.get();
    }

    public ObjectProperty<AutoRepairShopFx> autoRepairShopFxProperty() {
        return autoRepairShopFx;
    }

    public void setAutoRepairShopFx(AutoRepairShopFx autoRepairShopFx) {
        this.autoRepairShopFx.set(autoRepairShopFx);
    }

    public MechanicFx getMechanicFx() {
        return mechanicFx.get();
    }

    public ObjectProperty<MechanicFx> mechanicFxProperty() {
        return mechanicFx;
    }

    public void setMechanicFx(MechanicFx mechanicFx) {
        this.mechanicFx.set(mechanicFx);
    }

    public LocalDateTime getDateOfOrde() {
        return dateOfOrde.get();
    }

    public ObjectProperty<LocalDateTime> dateOfOrdeProperty() {
        return dateOfOrde;
    }

    public void setDateOfOrde(LocalDateTime dateOfOrde) {
        this.dateOfOrde.set(dateOfOrde);
    }

    public LocalDateTime getDateOdModify() {
        return dateOdModify.get();
    }

    public ObjectProperty<LocalDateTime> dateOdModifyProperty() {
        return dateOdModify;
    }

    public void setDateOdModify(LocalDateTime dateOdModify) {
        this.dateOdModify.set(dateOdModify);
    }

    @Override
    public String toString() {
        return dateOfOrde.getValue() + ", warsztat: " + autoRepairShopFx.toString() + ", samochód: " + carFx.toString();
    }
}
