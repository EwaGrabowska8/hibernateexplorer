package ModelsFx;

import ModelsSQL.DAO.DAO;
import ModelsSQL.Models.AutoRepairShopSQL;
import ModelsSQL.Models.CarSQL;
import ModelsSQL.Models.MechanicSQL;
import ModelsSQL.Models.RepairOrderSQL;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@NoArgsConstructor
@AllArgsConstructor
public class MechanicModel {
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("bundles.messages");

    private ObservableList<MechanicFx> observableList = FXCollections.observableArrayList();
    private ObservableList<MechanicFx> observableListArchive = FXCollections.observableArrayList();
    private static ObjectProperty<MechanicFx> objectProperty = new SimpleObjectProperty<>();
    private static ObjectProperty<MechanicFx> objectPropertyArchive = new SimpleObjectProperty<>();

    private ObservableList<AutoRepairShopFx> observableListAutoReoairShop = FXCollections.observableArrayList();
    private ObjectProperty<AutoRepairShopFx> autoRepairShopFxPredicate = new SimpleObjectProperty<>();

    private List<MechanicSQL> mechanicSQLArrayList = new ArrayList<>();
    private List<MechanicFx> mechanicFxArrayList = new ArrayList<>();
    private TreeItem<String> root = new TreeItem<>();

    public static ResourceBundle getResourceBundle() {
        return resourceBundle;
    }

    public static void setResourceBundle(ResourceBundle resourceBundle) {
        MechanicModel.resourceBundle = resourceBundle;
    }

    public ObservableList<MechanicFx> getObservableList() {
        return observableList;
    }

    public void setObservableList(ObservableList<MechanicFx> observableList) {
        this.observableList = observableList;
    }

    public ObservableList<MechanicFx> getObservableListArchive() {
        return observableListArchive;
    }

    public void setObservableListArchive(ObservableList<MechanicFx> observableListArchive) {
        this.observableListArchive = observableListArchive;
    }

    public static MechanicFx getObjectProperty() {
        return objectProperty.get();
    }

    public static ObjectProperty<MechanicFx> objectPropertyProperty() {
        return objectProperty;
    }

    public static void setObjectProperty(MechanicFx objectProperty) {
        MechanicModel.objectProperty.set(objectProperty);
    }

    public static MechanicFx getObjectPropertyArchive() {
        return objectPropertyArchive.get();
    }

    public static ObjectProperty<MechanicFx> objectPropertyArchiveProperty() {
        return objectPropertyArchive;
    }

    public static void setObjectPropertyArchive(MechanicFx objectPropertyArchive) {
        MechanicModel.objectPropertyArchive.set(objectPropertyArchive);
    }

    public ObservableList<AutoRepairShopFx> getObservableListAutoReoairShop() {
        return observableListAutoReoairShop;
    }

    public void setObservableListAutoReoairShop(ObservableList<AutoRepairShopFx> observableListAutoReoairShop) {
        this.observableListAutoReoairShop = observableListAutoReoairShop;
    }

    public AutoRepairShopFx getAutoRepairShopFxPredicate() {
        return autoRepairShopFxPredicate.get();
    }

    public ObjectProperty<AutoRepairShopFx> autoRepairShopFxPredicateProperty() {
        return autoRepairShopFxPredicate;
    }

    public void setAutoRepairShopFxPredicate(AutoRepairShopFx autoRepairShopFxPredicate) {
        this.autoRepairShopFxPredicate.setValue(autoRepairShopFxPredicate);
    }

    public List<MechanicSQL> getMechanicSQLArrayList() {
        return mechanicSQLArrayList;
    }

    public void setMechanicSQLArrayList(List<MechanicSQL> mechanicSQLArrayList) {
        this.mechanicSQLArrayList = mechanicSQLArrayList;
    }

    public TreeItem<String> getRoot() {
        return root;
    }

    public void setRoot(TreeItem<String> root) {
        this.root = root;
    }

    public void initList() {
        mechanicSQLArrayList = DAO.getAll(MechanicSQL.class);
        observableList.clear();
        mechanicSQLArrayList.forEach(mechanic -> {
            if (!mechanic.isArchival()) {
                MechanicFx mechanicFx = convert_mechanic_to_mechanicFx(mechanic);
                mechanicFxArrayList.add(mechanicFx);
            } else {
                observableListArchive.add(convert_mechanic_to_mechanicFx(mechanic));
            }
        });
        this.observableList.setAll(this.mechanicFxArrayList);
    }

    public static MechanicFx convert_mechanic_to_mechanicFx(MechanicSQL mechanicSQL) {
        MechanicFx mechanicFx = new MechanicFx();
        if (mechanicSQL.getAutoRepairShopSQL() != null) {
            AutoRepairShopSQL autoRepairShopSQL = mechanicSQL.getAutoRepairShopSQL();
            AutoRepairShopFx autoRepairShopFx = AutoRepairShopModel.convert_autoRepairShop_to_autoRepairShopFx(autoRepairShopSQL);
            mechanicFx.setAutoRepairShopFx(autoRepairShopFx);
        }
        mechanicFx.setId(mechanicSQL.getId());
        mechanicFx.setName(mechanicSQL.getName());
        mechanicFx.setSurname(mechanicSQL.getSurname());
        mechanicFx.setPesel(mechanicSQL.getPesel());
        return mechanicFx;
    }

    public static MechanicSQL convert_mechanicFx_to_mechanic(MechanicFx mechanicFx) {
        MechanicSQL mechanicSQL = new MechanicSQL();
        if (mechanicFx.getAutoRepairShopFx() != null) {
            AutoRepairShopFx autoRepairShopFx = mechanicFx.getAutoRepairShopFx();
            AutoRepairShopSQL autoRepairShopSQL = AutoRepairShopModel.convert_autoRepairShopFx_to_autoRepairShop(autoRepairShopFx);
            mechanicSQL.setAutoRepairShopSQL(autoRepairShopSQL);
        }
        mechanicSQL.setId(mechanicFx.getId());
        mechanicSQL.setName(mechanicFx.getName());
        mechanicSQL.setSurname(mechanicFx.getSurname());
        mechanicSQL.setPesel(mechanicFx.getPesel());
        return mechanicSQL;
    }

    public void addMechanic(long pesel, String name, String surname, AutoRepairShopFx autoRepairShopFx) {
        MechanicSQL mechanicSQL = new MechanicSQL(name, surname, pesel);
        DAO.saveOrUpdateEntity(mechanicSQL);
        if (autoRepairShopFx != null) {
            Optional<AutoRepairShopSQL> autoRepairShopOptional = DAO.getEntityById(autoRepairShopFx.getId(), AutoRepairShopSQL.class);
            List<MechanicSQL> allMechanicSQLS = DAO.getAll(MechanicSQL.class);
            DAO.addRelation_AutoRepairShop_Mechanic(autoRepairShopOptional.get().getId(), allMechanicSQLS.get(allMechanicSQLS.size() - 1).getId());
        }
    }

    public void initRoot() {
        this.mechanicSQLArrayList.forEach(mechanic -> {
            if (!mechanic.isArchival()) {
                setItemMechani(mechanic);
            }
        });
    }

    public void setItemMechani(MechanicSQL mechanic) {
        String mechanicToString = mechanic.getSurname() + " " + mechanic.getName() + ", " + mechanic.getPesel();
        TreeItem<String> mechanicItem = new TreeItem<>(mechanicToString);
        this.root.getChildren().add(mechanicItem);

        String carsItemTitle = resourceBundle.getString("cars");
        TreeItem<String> carsItem = new TreeItem<>(carsItemTitle);
        mechanicItem.getChildren().add(carsItem);

        Set<CarSQL> carSQLSet = mechanic.getCarSQLSet();
        carSQLSet.forEach(carSQL -> {
            if (!carSQL.isArchival()) {
                setItemCar(carsItem, carSQL);
            }
        });

        String ordersItemTitle = resourceBundle.getString("ordersTB");
        TreeItem<String> orderssItem = new TreeItem<>(ordersItemTitle);
        mechanicItem.getChildren().add(orderssItem);

        Set<RepairOrderSQL> repairOrderSQLSet = mechanic.getRepairOrderSQLSet();
        repairOrderSQLSet.forEach(order -> {
            String singleOrderToString = order.getDateOfOrder() + ", " + order.getCarSQL().getRegistrationNumber();
            TreeItem<String> singleOrder = new TreeItem<>(singleOrderToString);
            orderssItem.getChildren().add(singleOrder);
        });
    }

    public void setItemCar(TreeItem<String> carsItem, CarSQL carSQL) {
        String singleCarToString = carSQL.getBrand() + " " + carSQL.getModel() + ", " + carSQL.getRegistrationNumber();
        TreeItem<String> singleCar = new TreeItem<>(singleCarToString);
        carsItem.getChildren().add(singleCar);
    }

    public void editMechanic(long pesel, String name, String surname, AutoRepairShopFx autoRepairShopFx, long id) {
        MechanicSQL mechanicSQL = findMechanicById(id);
        mechanicSQL.setPesel(pesel);
        mechanicSQL.setName(name);
        mechanicSQL.setSurname(surname);

        AutoRepairShopSQL autoRepairShopSQL = null;
        if (autoRepairShopFx != null) {
            autoRepairShopSQL = AutoRepairShopModel.convert_autoRepairShopFx_to_autoRepairShop(autoRepairShopFx);
        }
        mechanicSQL.setAutoRepairShopSQL(autoRepairShopSQL);

        DAO.saveOrUpdateEntity(mechanicSQL);
    }

    public MechanicSQL findMechanicById(long id) {
        Optional<MechanicSQL> entityById = DAO.getEntityById(id, MechanicSQL.class);
        if (entityById.isPresent()) {
            return entityById.get();
        }
        return null;
    }

    public void setIsArchiveAndRemoveRelation() {
        long id = objectProperty.get().getId();
        MechanicSQL mechanicToUpdate = findMechanicById(id);
        this.setIsArchive(mechanicToUpdate);
        this.removeAllRelation(mechanicToUpdate);
    }

    private void removeAllRelation(MechanicSQL mechanicToUpdate) {
        Set<CarSQL> carSQLSet = mechanicToUpdate.getCarSQLSet();
        long idMechanic = mechanicToUpdate.getId();
        carSQLSet.forEach(carSQL -> {
            long idCar = carSQL.getId();
            DAO.removeRelation_Car_Mechanic(idCar, idMechanic);
        });
        AutoRepairShopSQL autoRepairShopSQL = mechanicToUpdate.getAutoRepairShopSQL();
        if (autoRepairShopSQL != null) {
            long idAutoRepairShop = autoRepairShopSQL.getId();
            DAO.removeRelation_AutoRepairShop_Mechanic(idAutoRepairShop, idMechanic);
        }
    }


    public void setIsArchive(MechanicSQL mechanicToUpdate) {
        mechanicToUpdate.setArchival(true);
        DAO.saveOrUpdateEntity(mechanicToUpdate);
    }

    public void filterObservableListByPredicate(){
        if (getAutoRepairShopFxPredicate()!=null){
            List<MechanicFx> filteredList = this.mechanicFxArrayList.stream()
                    .filter(getPredicate(autoRepairShopFxPredicate.get()))
                    .collect(Collectors.toList());
            this.observableList.setAll(filteredList);
        }
        else {
            this.observableList.setAll(mechanicFxArrayList);
        }

    }

    private Predicate<MechanicFx> getPredicate(AutoRepairShopFx autoRepairShopFx){
        return mechanicFx -> mechanicFx.getAutoRepairShopFx().getId()==autoRepairShopFx.getId();
    }
}
