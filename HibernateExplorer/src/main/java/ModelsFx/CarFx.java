package ModelsFx;

import ModelsSQL.Models.CarBodyType;
import javafx.beans.property.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
public class CarFx {
    private StringProperty registrationNumber = new SimpleStringProperty();
    private LongProperty id = new SimpleLongProperty();
    private StringProperty brand = new SimpleStringProperty();
    private StringProperty model = new SimpleStringProperty();
    private ObjectProperty<CarBodyType> carBodyType = new SimpleObjectProperty<>();
    private ObjectProperty<LocalDate> productionDate = new SimpleObjectProperty<>();
    private ObjectProperty<LocalDateTime> modifiedDate = new SimpleObjectProperty<>();
    private StringProperty color = new SimpleStringProperty();
    private ObjectProperty<Long> km = new SimpleObjectProperty<>();
    private ObjectProperty<MechanicFx> mechanic = new SimpleObjectProperty<>();
    private BooleanProperty isArchive = new SimpleBooleanProperty();

    public String getRegistrationNumber() {
        return registrationNumber.get();
    }

    public StringProperty registrationNumberProperty() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber.set(registrationNumber);
    }

    public long getId() {
        return id.get();
    }

    public LongProperty idProperty() {
        return id;
    }

    public void setId(long id) {
        this.id.set(id);
    }

    public String getBrand() {
        return brand.get();
    }

    public StringProperty brandProperty() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand.set(brand);
    }

    public String getModel() {
        return model.get();
    }

    public StringProperty modelProperty() {
        return model;
    }

    public void setModel(String model) {
        this.model.set(model);
    }

    public CarBodyType getCarBodyType() {
        return carBodyType.get();
    }

    public ObjectProperty<CarBodyType> carBodyTypeProperty() {
        return carBodyType;
    }

    public void setCarBodyType(CarBodyType carBodyType) {
        this.carBodyType.set(carBodyType);
    }

    public LocalDate getProductionDate() {
        return productionDate.get();
    }

    public ObjectProperty<LocalDate> productionDateProperty() {
        return productionDate;
    }

    public void setProductionDate(LocalDate productionDate) {
        this.productionDate.set(productionDate);
    }

    public LocalDateTime getModifiedDate() {
        return modifiedDate.get();
    }

    public ObjectProperty<LocalDateTime> modifiedDateProperty() {
        return modifiedDate;
    }

    public void setModifiedDate(LocalDateTime modifiedDate) {
        this.modifiedDate.set(modifiedDate);
    }

    public String getColor() {
        return color.get();
    }

    public StringProperty colorProperty() {
        return color;
    }

    public void setColor(String color) {
        this.color.set(color);
    }

    public Long getKm() {
        return km.get();
    }

    public ObjectProperty<Long> kmProperty() {
        return km;
    }

    public void setKm(Long km) {
        this.km.set(km);
    }

    public MechanicFx getMechanic() {
        return mechanic.get();
    }

    public ObjectProperty<MechanicFx> mechanicProperty() {
        return mechanic;
    }

    public void setMechanic(MechanicFx mechanic) {
        this.mechanic.set(mechanic);
    }

    public boolean isIsArchive() {
        return isArchive.get();
    }

    public BooleanProperty isArchiveProperty() {
        return isArchive;
    }

    public void setIsArchive(boolean isArchive) {
        this.isArchive.set(isArchive);
    }

    @Override
    public String toString() {
        return registrationNumber.getValue() + ", \n" + brand.getValue() + " " + model.getValue();
    }
}
