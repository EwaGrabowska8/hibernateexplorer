package ModelsFx;

import ModelsSQL.Models.CarBodyType;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Arrays;

@NoArgsConstructor
@AllArgsConstructor
public class CarBodyTypeModel {
    private ObservableList<String> observableList = FXCollections.observableArrayList();

    public ObservableList<String> getObservableList() {
        return observableList;
    }

    public void setObservableList(ObservableList<String> observableList) {
        this.observableList = observableList;
    }

    public void initList() {
        Arrays.stream(CarBodyType.values())
                .forEach(v -> {
                    observableList.add(v.toString());
                });

    }
}
