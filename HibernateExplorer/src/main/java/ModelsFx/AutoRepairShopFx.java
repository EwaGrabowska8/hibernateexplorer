package ModelsFx;

import javafx.beans.property.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
public class AutoRepairShopFx implements Serializable {
    private LongProperty id = new SimpleLongProperty();
    private StringProperty name = new SimpleStringProperty();
    private StringProperty adress = new SimpleStringProperty();
    private LongProperty NIP = new SimpleLongProperty();
    private BooleanProperty isArchive = new SimpleBooleanProperty();

    public long getId() {
        return id.get();
    }

    public LongProperty idProperty() {
        return id;
    }

    public void setId(long id) {
        this.id.set(id);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getAdress() {
        return adress.get();
    }

    public StringProperty adressProperty() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress.set(adress);
    }

    public long getNIP() {
        return NIP.get();
    }

    public LongProperty NIPProperty() {
        return NIP;
    }

    public void setNIP(long NIP) {
        this.NIP.set(NIP);
    }

    public boolean isIsArchive() {
        return isArchive.get();
    }

    public BooleanProperty isArchiveProperty() {
        return isArchive;
    }

    public void setIsArchive(boolean isArchive) {
        this.isArchive.set(isArchive);
    }

    @Override
    public String toString() {
        return name.getValue() + ", \nNIP: " + NIP.getValue();
    }
}
