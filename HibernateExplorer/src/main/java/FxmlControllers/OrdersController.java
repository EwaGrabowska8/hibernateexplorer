package FxmlControllers;

import ModelsFx.*;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

public class OrdersController {
    @FXML
    private VBox Orders;
    @FXML
    private DatePicker fromDatePicker;
    @FXML
    private DatePicker toDatePicker;
    @FXML
    private ToggleGroup status;
    @FXML
    private RadioButton finished;
    @FXML
    private RadioButton inProgres;
    @FXML
    private RadioButton all;
    @FXML
    private ComboBox<CarFx> carComboBox;
    @FXML
    private ComboBox<AutoRepairShopFx> autoRepaiShopComboBox;
    @FXML
    private ComboBox<MechanicFx> mechanicComboBox;
    @FXML
    private TableView<RepairOrderFx> tableOrders;
    @FXML
    private TableColumn<RepairOrderFx, LocalDateTime> dateOfAddOrder;
    @FXML
    private TableColumn<RepairOrderFx, LocalDateTime> dateOdLastModyfyOrder;
    @FXML
    private TableColumn<RepairOrderFx, CarFx> carOrder;
    @FXML
    private TableColumn<RepairOrderFx, AutoRepairShopFx> autoRepaiOrder;
    @FXML
    private TableColumn<RepairOrderFx, MechanicFx> mechanicOrder;
    @FXML
    private TableColumn<RepairOrderFx, String> descriptionOrder;
    @FXML
    private TableColumn<RepairOrderFx, Boolean> endedColumn;

    private RepairOrderModel repairOrderModel;
    private CarModel carModel;
    private AutoRepairShopModel autoRepairShopModel;
    private MechanicModel mechanicModel;

    @FXML
    public void initialize() {
        this.repairOrderModel = new RepairOrderModel();
        this.carModel = new CarModel();
        this.autoRepairShopModel = new AutoRepairShopModel();
        this.mechanicModel = new MechanicModel();

        this.repairOrderModel.initList();

        setTableView();
        this.tableOrders.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                RepairOrderFx selectedItem = tableOrders.getSelectionModel().getSelectedItem();
                this.repairOrderModel.setObjectProperty(selectedItem);
                ViewBuilder.hideOnlyButtonArchive();
            }
        });
        ViewBuilder.showOnlyAddButtonLeft();
        this.setFilterControls();
    }

    private void setFilterControls() {
        this.carModel.initList();
        this.carComboBox.setItems(this.carModel.getObservableList());
        this.autoRepairShopModel.initList();
        this.autoRepaiShopComboBox.setItems(autoRepairShopModel.getObservableList());
        this.mechanicModel.initList();
        this.mechanicComboBox.setItems(mechanicModel.getObservableList());
    }

    private void setTableView() {
        ObservableList<RepairOrderFx> observableList = repairOrderModel.getObservableList();
        this.tableOrders.setItems(observableList);
        this.dateOfAddOrder.setCellValueFactory(cell -> cell.getValue().dateOfOrdeProperty());
        this.dateOdLastModyfyOrder.setCellValueFactory(cell -> cell.getValue().dateOdModifyProperty());
        this.carOrder.setCellValueFactory(cell -> cell.getValue().carFxProperty());
        this.autoRepaiOrder.setCellValueFactory(cell -> cell.getValue().autoRepairShopFxProperty());
        this.mechanicOrder.setCellValueFactory(cell -> cell.getValue().mechanicFxProperty());
        this.descriptionOrder.setCellValueFactory(cell -> cell.getValue().descriptionProperty());
        this.endedColumn.setCellFactory(CheckBoxTableCell.forTableColumn(param -> tableOrders.getItems().get(param).repairedProperty()));
        setWrapCellFactory(descriptionOrder);
    }

    private void setWrapCellFactory(TableColumn<RepairOrderFx, String> column) {
        column.setCellFactory(tablecol -> {
            TableCell<RepairOrderFx, String> cell = new TableCell<>();
            Text text = new Text();
            cell.setGraphic(text);
            text.wrappingWidthProperty().bind(cell.widthProperty());
            text.textProperty().bind(cell.itemProperty());
            return cell;
        });
    }

    public void cancelFilterFrom(ActionEvent actionEvent) {
        this.fromDatePicker.getEditor().clear();
        this.filterTableView();
    }

    public void cancelFilterTo(ActionEvent actionEvent) {this.repairOrderModel.initList();
        this.toDatePicker.getEditor().clear();
        this.filterTableView();
    }

    public void cancelFilterCar(ActionEvent actionEvent) {
        this.carComboBox.getSelectionModel().clearSelection();
        this.filterTableView();
    }

    public void cancelFilterAutoRepairShop(ActionEvent actionEvent) {
        this.autoRepaiShopComboBox.getSelectionModel().clearSelection();
        this.filterTableView();
    }

    public void cancelFilterMechanic(ActionEvent actionEvent) {
        this.mechanicComboBox.getSelectionModel().clearSelection();
        this.filterTableView();
    }

    public void filterTableView() {
        Optional<LocalDate> from = Optional.ofNullable(this.fromDatePicker.getValue());
        Optional<LocalDate> to = Optional.ofNullable(this.toDatePicker.getValue());
        Boolean selectedStatus = this.setSelectedStatus();
        Optional<CarFx> carFx = Optional.ofNullable(this.carComboBox.getValue());
        Optional<AutoRepairShopFx> autoRepairShopFx = Optional.ofNullable(this.autoRepaiShopComboBox.getValue());
        Optional<MechanicFx> mechanicFx = Optional.ofNullable(this.mechanicComboBox.getValue());

        this.repairOrderModel.filterOvservableListByPredicats(from, to, selectedStatus, carFx, autoRepairShopFx, mechanicFx);
    }

    private Boolean setSelectedStatus() {
        if (all.isSelected()){
            return null;
        }else
        if (inProgres.isSelected()){
            return false;
        }else{
            return true;
        }

    }
}
