package FxmlControllers;

import Dialogs.DialogsMenuBar;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.print.*;
import javafx.scene.Scene;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.transform.Scale;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Locale;
import java.util.Optional;

public class MainController {

    @FXML
    private BorderPane mainBorderPane;
    private Stage stage;

    @FXML
    private BorderPaneTopController borderPaneTopController;

    @FXML
    private BorderPaneLeftController borderPaneLeftController;

    @FXML
    void initialize() throws IOException {
        borderPaneTopController.setMainController(this);
        borderPaneTopController.setBorderPaneLeft(borderPaneLeftController);
        ViewBuilder.setMainBorderPane(mainBorderPane);
        ViewBuilder.setBorderPaneLeft(borderPaneLeftController);
    }

    public void print(ActionEvent event) {
        PrinterJob job = PrinterJob.createPrinterJob();

        if (job != null && job.showPrintDialog(mainBorderPane.getScene().getWindow())) {
            Printer printer = job.getPrinter();
            PageLayout pageLayout = printer.createPageLayout(Paper.A4, PageOrientation.LANDSCAPE, Printer.MarginType.HARDWARE_MINIMUM);

            mainBorderPane.getTop().setVisible(false);
            mainBorderPane.getLeft().setVisible(false);
            mainBorderPane.getTop().setManaged(false);
            mainBorderPane.getLeft().setManaged(false);

            double width = mainBorderPane.getWidth();
            double height = mainBorderPane.getHeight();

            PrintResolution resolution = job.getJobSettings().getPrintResolution();

            width /= resolution.getFeedResolution();
            height /= resolution.getCrossFeedResolution();

            double scaleX = pageLayout.getPrintableWidth() / width / 600;
            double scaleY = pageLayout.getPrintableHeight() / height / 600;

            Scale scale = new Scale(scaleX, scaleY);

            mainBorderPane.getTransforms().add(scale);

            boolean success = job.printPage(pageLayout, mainBorderPane);
            if (success) {
                job.endJob();
            }
            mainBorderPane.getTransforms().remove(scale);
        }
        mainBorderPane.getTop().setManaged(true);
        mainBorderPane.getLeft().setManaged(true);
        mainBorderPane.getTop().setVisible(true);
        mainBorderPane.getLeft().setVisible(true);

    }

    public void closeAplication(ActionEvent event) {
        Optional<ButtonType> result = DialogsMenuBar.confirmationExitDialog();
        ButtonType buttonType = result.get();
        if (buttonType == ButtonType.OK) {
            Platform.exit();
            System.exit(0);
        }
    }

    public void setCaspian(ActionEvent event) {
        Application.setUserAgentStylesheet(Application.STYLESHEET_CASPIAN);
    }

    public void setModena(ActionEvent event) {
        Application.setUserAgentStylesheet(Application.STYLESHEET_MODENA);
    }

    public void setAlwaysOnTop(ActionEvent event) {
        stage = (Stage) mainBorderPane.getScene().getWindow();
        boolean isSelected = ((CheckMenuItem) event.getSource()).selectedProperty().get();
        stage.setAlwaysOnTop(isSelected);
    }

    public void setOpacity_100(ActionEvent event) {
        stage = (Stage) mainBorderPane.getScene().getWindow();
        stage.setOpacity(1.0);
    }

    public void setOpacity_75(ActionEvent event) {
        stage = (Stage) mainBorderPane.getScene().getWindow();
        stage.setOpacity(0.75);
    }

    public void setOpacity_50(ActionEvent event) {
        stage = (Stage) mainBorderPane.getScene().getWindow();
        stage.setOpacity(0.5);
    }

    public void showDatabaeSchema(ActionEvent actionEvent) {
        ViewBuilder.setCenter(DatabaseSchemaController.DATABASE_SCHEMA_FXML);
        ViewBuilder.hideAllButtonLeft();
    }
}
