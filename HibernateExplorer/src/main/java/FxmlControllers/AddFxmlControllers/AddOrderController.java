package FxmlControllers.AddFxmlControllers;

import Dialogs.DialogsErrorAplication;
import Dialogs.DialogsInformation;
import FxmlControllers.BorderPaneTopController;
import FxmlControllers.ViewBuilder;
import ModelsFx.*;
import ModelsSQL.DAO.DAO;
import ModelsSQL.Models.CarSQL;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;

public class AddOrderController {

    public static final String ADD_ORDER_FXML = "/Fxml/AddFxml/AddOrder.fxml";

    @FXML
    private VBox AddOrder;
    @FXML
    private ComboBox<CarFx> comboBoxCar;
    @FXML
    private ComboBox<AutoRepairShopFx> comboBoxAutoRepairShop;
    @FXML
    private ComboBox<MechanicFx> comboBoxMechanic;
    @FXML
    private TextArea textAreaDescription;
    @FXML
    private Button okButton;

    private RepairOrderModel repairOrderModel;
    private CarModel carModel;
    private AutoRepairShopModel autoRepairShopModel;
    private MechanicModel mechanicModel;

    @FXML
    public void initialize() {
        this.repairOrderModel = new RepairOrderModel();
        this.carModel = new CarModel();
        this.autoRepairShopModel = new AutoRepairShopModel();
        this.mechanicModel = new MechanicModel();

        setControls();
        setBindings();
    }

    private void setBindings() {
        comboBoxCar.disableProperty().bind(textAreaDescription.textProperty().isEmpty());
        comboBoxAutoRepairShop.disableProperty().bind(comboBoxCar.valueProperty().isNull());
        comboBoxMechanic.disableProperty().bind(comboBoxAutoRepairShop.valueProperty().isNull());
        okButton.disableProperty().bind(comboBoxMechanic.valueProperty().isNull());
    }

    private void setControls() {
        textAreaDescription.clear();

        carModel.initList();
        ObservableList<CarFx> carModelObservableList = carModel.getObservableList();
        comboBoxCar.setItems(carModelObservableList);

        autoRepairShopModel.initList();
        ObservableList<AutoRepairShopFx> autoRepairShopModelObservableList = autoRepairShopModel.getObservableList();
        comboBoxAutoRepairShop.setItems(autoRepairShopModelObservableList);

        mechanicModel.initList();
        ObservableList<MechanicFx> mechanicModelObservableList = mechanicModel.getObservableList();
        comboBoxMechanic.setItems(mechanicModelObservableList);
    }

    public void addOrder(ActionEvent event) {
        try {
            repairOrderModel.addRepairOrder
                    (textAreaDescription.getText(), comboBoxCar.getValue(), comboBoxAutoRepairShop.getValue(), comboBoxMechanic.getValue());
            CarSQL carSQL = CarModel.convert_carFx_to_car(comboBoxCar.getValue());
            DialogsInformation.addSukcess();
        } catch (Exception e) {
            DialogsErrorAplication.daoExceptionDialog(e);
        }
        ViewBuilder.setCenter(BorderPaneTopController.ORDERS_FXML);
    }
}
