package FxmlControllers.AddFxmlControllers;

import Dialogs.DialogsErrorAplication;
import Dialogs.DialogsInformation;
import FxmlControllers.BorderPaneTopController;
import FxmlControllers.MyUniversalTools;
import FxmlControllers.ViewBuilder;
import ModelsFx.AutoRepairShopModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

public class AddAutoRepairShopController {

    @FXML
    private VBox AddAutoRepairShop;
    @FXML
    private TextField nameAutoRepairShop;
    @FXML
    private TextField adressAutoRepairShop;
    @FXML
    private TextField nipAutoRepairShop;
    @FXML
    private Button okButton;

    private AutoRepairShopModel autoRepairShopModel;
    public static final String ADD_AUTO_REPAIR_SHOP_FXML = "/Fxml/AddFxml/AddAutoRepairShop.fxml";

    public void initialize() {
        autoRepairShopModel = new AutoRepairShopModel();
        setControls();
        setBindings();
    }

    public void setControls() {
        nameAutoRepairShop.clear();
        adressAutoRepairShop.clear();
        nipAutoRepairShop.clear();
    }

    private void setBindings() {
        adressAutoRepairShop.disableProperty().bind(nameAutoRepairShop.textProperty().isEmpty());
        nipAutoRepairShop.disableProperty().bind(adressAutoRepairShop.textProperty().isEmpty());
        okButton.disableProperty().bind(nipAutoRepairShop.textProperty().isEmpty());
    }

    public void addAutoRepairShop(ActionEvent event) {
        if (nipIsCorrect()) {
            try {
                long nip = Long.parseLong(nipAutoRepairShop.getText());
                autoRepairShopModel.addAutoRepairShop(nameAutoRepairShop.getText(), adressAutoRepairShop.getText(), nip);
                DialogsInformation.addSukcess();
            } catch (Exception e) {
                DialogsErrorAplication.daoExceptionDialog(e);
            }
            ViewBuilder.setCenter(BorderPaneTopController.AUTO_REPAIR_SHOP_FXML);
        }
    }

    public boolean nipIsCorrect() {
        String nipToChect = nipAutoRepairShop.getText().trim();
        boolean isLong = MyUniversalTools.isLong(nipToChect);
        if (!isLong) {
            return false;
        }
        boolean isGoodLenght = MyUniversalTools.isGoodLenght(nipToChect, 10);
        if (!isGoodLenght) {
            return false;
        }
        return true;
    }

}
