package FxmlControllers.AddFxmlControllers;

import Dialogs.DialogsErrorAplication;
import Dialogs.DialogsInformation;
import FxmlControllers.BorderPaneTopController;
import FxmlControllers.MyUniversalTools;
import FxmlControllers.ViewBuilder;
import ModelsFx.AutoRepairShopFx;
import ModelsFx.AutoRepairShopModel;
import ModelsFx.MechanicModel;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

public class AddMechanicController {

    @FXML
    private VBox AddMechanic;
    @FXML
    private TextField name;
    @FXML
    private TextField surname;
    @FXML
    private TextField pesel;
    @FXML
    private ComboBox<AutoRepairShopFx> comboBox;
    @FXML
    private Button okButton;

    private MechanicModel mechanicModel;
    private AutoRepairShopModel autoRepairShopModel;
    private SimpleBooleanProperty simpleBooleanProperty;

    public static final String ADD_MECHANIC_FXML = "/Fxml/AddFxml/AddMechanic.fxml";

    @FXML
    public void initialize() {
        this.mechanicModel = new MechanicModel();
        this.autoRepairShopModel = new AutoRepairShopModel();
        setControls();
        setBindings();
    }

    public void setControls() {
        name.clear();
        surname.clear();
        pesel.clear();
        autoRepairShopModel.initList();
        comboBox.setItems(autoRepairShopModel.getObservableList());
    }

    private void setBindings() {
        surname.disableProperty().bind(name.textProperty().isEmpty());
        pesel.disableProperty().bind(surname.textProperty().isEmpty());
        comboBox.disableProperty().bind(pesel.textProperty().isEmpty());
        okButton.disableProperty().bind(pesel.textProperty().isEmpty());
    }

    public void addMechanic(ActionEvent event) {
        if (peselIsCorrect()) {
            long peselNumber = Long.parseLong(pesel.getText());
            try {
                mechanicModel.addMechanic(peselNumber, name.getText(), surname.getText(), comboBox.getValue());
                DialogsInformation.addSukcess();
            } catch (Exception e) {
                DialogsErrorAplication.daoExceptionDialog(e);
            }
            ViewBuilder.setCenter(BorderPaneTopController.MECHANICS_FXML);
        }
    }

    public boolean peselIsCorrect() {
        String peselToChect = pesel.getText().trim();
        boolean isLong = MyUniversalTools.isLong(peselToChect);
        if (!isLong) {
            return false;
        }
        boolean isGoodLenght = MyUniversalTools.isGoodLenght(peselToChect, 11);
        if (!isGoodLenght) {
            return false;
        }
        return true;
    }
}
