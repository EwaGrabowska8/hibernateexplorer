package FxmlControllers.AddFxmlControllers;

import ModelsSQL.Models.CarBodyType;
import Dialogs.DialogsErrorAplication;
import Dialogs.DialogsInformation;
import FxmlControllers.BorderPaneTopController;
import FxmlControllers.MyUniversalTools;
import FxmlControllers.ViewBuilder;
import ModelsFx.CarBodyTypeModel;
import ModelsFx.CarModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

public class AddCarController {

    @FXML
    private VBox AddCar;
    @FXML
    private TextField registrationNumber;
    @FXML
    private TextField brand;
    @FXML
    private TextField model;
    @FXML
    private DatePicker dateOfProduction;
    @FXML
    private TextField km;
    @FXML
    private ComboBox<String> carBodyType;
    @FXML
    private TextField color;
    @FXML
    private Button okButton;

    public static final String ADD_CAR_FXML = "/Fxml/AddFxml/AddCar.fxml";

    private CarModel carModel;
    private CarBodyTypeModel carBodyTypeModel;

    @FXML
    public void initialize() {
        this.carModel = new CarModel();
        this.carBodyTypeModel = new CarBodyTypeModel();

        setControls();
        setBindings();
    }

    public void setControls() {
        this.registrationNumber.clear();
        this.brand.clear();
        this.model.clear();
        this.km.clear();
        this.color.clear();

        carBodyTypeModel.initList();
        carBodyType.setItems(carBodyTypeModel.getObservableList());
    }

    private void setBindings() {

        dateOfProduction.disableProperty().bind(registrationNumber.textProperty().isEmpty());
        brand.disableProperty().bind(dateOfProduction.valueProperty().isNull());
        model.disableProperty().bind(brand.textProperty().isEmpty());
        km.disableProperty().bind(model.textProperty().isEmpty());
        carBodyType.disableProperty().bind(km.textProperty().isEmpty());
        color.disableProperty().bind(carBodyType.valueProperty().isNull());
        okButton.disableProperty().bind(color.textProperty().isEmpty());
    }

    public void addCar(ActionEvent event) {
        String kmString = km.getText();
        if (kmIsCorrect(kmString)) {
            try {
                carModel.addCar(registrationNumber.getText(), brand.getText(), model.getText(), CarBodyType.valueOf(carBodyType.getValue()), dateOfProduction.getValue(), color.getText(), Long.parseLong(km.getText()));
                DialogsInformation.addSukcess();
            } catch (Exception e) {
                DialogsErrorAplication.daoExceptionDialog(e);
            }
            ViewBuilder.setCenter(BorderPaneTopController.CARS_FXML);
        }
    }

    public boolean kmIsCorrect(String km) {
        return MyUniversalTools.isLong(km);
    }
}
