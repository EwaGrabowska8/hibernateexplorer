package FxmlControllers;


import ModelsFx.AutoRepairShopFx;
import ModelsFx.AutoRepairShopModel;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;

public class AutoRepairShopController {
    @FXML
    private VBox AutoRepaisSchops;
    @FXML
    private Tab tableTabAutoRepaiShop;
    @FXML
    private TableView<AutoRepairShopFx> tableAutoRepairShop;
    @FXML
    private TableColumn<AutoRepairShopFx, String> nameAutoRepairShopColumn;
    @FXML
    private TableColumn<AutoRepairShopFx, String> adressAutoRepairShopColumn;
    @FXML
    private TableColumn<AutoRepairShopFx, Long> nipColumn;
    @FXML
    private TreeView<String> treeViewAutoRepairShop;
    @FXML
    private Tab treeTabAutoRepaiShop;

    private AutoRepairShopModel autoRepairShopModel;

    public void initialize() {
        this.autoRepairShopModel = new AutoRepairShopModel();

        this.autoRepairShopModel.initList();
        this.autoRepairShopModel.initRoot();
        this.treeViewAutoRepairShop.setRoot(this.autoRepairShopModel.getRoot());

        this.tableAutoRepairShop.setItems(autoRepairShopModel.getObservableList());
        this.nameAutoRepairShopColumn.setCellValueFactory(cell -> cell.getValue().nameProperty());
        this.adressAutoRepairShopColumn.setCellValueFactory(cell -> cell.getValue().adressProperty());
        this.nipColumn.setCellValueFactory(cell -> cell.getValue().NIPProperty().asObject());
        this.tableAutoRepairShop.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                AutoRepairShopFx selectedItem = tableAutoRepairShop.getSelectionModel().getSelectedItem();
                this.autoRepairShopModel.setObjectProperty(selectedItem);
                ViewBuilder.showAllButtonLeft();
            }
        });
        ViewBuilder.showOnlyAddButtonLeft();
    }

    public void treeTabisSelected(Event event) {
        ViewBuilder.showOnlyAddButtonLeft();
    }

    public void moveToArchive() {

    }
}
