package FxmlControllers;

import javafx.fxml.FXML;
import javafx.scene.layout.VBox;

public class DatabaseSchemaController {
    public static final String DATABASE_SCHEMA_FXML = "/Fxml/DatabaseSchema.fxml";
    @FXML
    private VBox databaseSchema;
}
