package FxmlControllers;

import Dialogs.DialogsErrorAplication;

public class MyUniversalTools {

    public static boolean isGoodLenght(String inputValue, int expectedLenght) {
        int length = inputValue.trim().length();
        if (length == expectedLenght) {
            return true;
        } else {
            DialogsErrorAplication.lenghtExceptionDialog(inputValue, expectedLenght);
            return false;
        }
    }

    public static boolean isLong(String valueToParse) {
        try {
            Long.parseLong(valueToParse);
            return true;
        } catch (NumberFormatException e) {
            DialogsErrorAplication.LongParseExceptionDialog(valueToParse);
            return false;
        }
    }
}
