package FxmlControllers;

import ModelsFx.*;
import ModelsSQL.Models.MechanicSQL;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import lombok.RequiredArgsConstructor;


public class MechanicsController {

    @FXML
    private VBox mechanics;
    @FXML
    private ComboBox<AutoRepairShopFx> companyCombobox;
    @FXML
    private Tab tableTabMechanic;
    @FXML
    private TableView<MechanicFx> tableMechanic;
    @FXML
    private TableColumn<MechanicFx, String> nameColumn;
    @FXML
    private TableColumn<MechanicFx, String> surnameColumn;
    @FXML
    private TableColumn<MechanicFx, Long> peselColumn;
    @FXML
    private TableColumn<MechanicFx, AutoRepairShopFx> employmentPlace;
    @FXML
    private TreeView<String> treeViewMechanic;
    @FXML
    private Tab treeTabMechanic;

    private MechanicModel mechanicModel;
    private AutoRepairShopModel autoRepairShopModel;

    @FXML
    public void initialize() {
        this.mechanicModel = new MechanicModel();
        this.autoRepairShopModel = new AutoRepairShopModel();

        this.mechanicModel.initList();
        setTreeView();
        setTableView(mechanicModel.getObservableList());
        this.tableMechanic.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                MechanicFx selectedItem = tableMechanic.getSelectionModel().getSelectedItem();
                this.mechanicModel.setObjectProperty(selectedItem);
                ViewBuilder.showAllButtonLeft();
            }
        });
        ViewBuilder.showOnlyAddButtonLeft();
        setCompanyComboBox();
    }

    private void setCompanyComboBox() {
        this.autoRepairShopModel.initList();
        ObservableList<AutoRepairShopFx> autoRepairShopList = this.autoRepairShopModel.getObservableList();
        this.companyCombobox.setItems(autoRepairShopList);
    }

    public void setTableView(ObservableList<MechanicFx> mechanicFxObservableList) {
        this.tableMechanic.setItems(mechanicFxObservableList);
        this.mechanicModel.autoRepairShopFxPredicateProperty().bind(companyCombobox.valueProperty());
        this.nameColumn.setCellValueFactory(cell -> cell.getValue().nameProperty());
        this.surnameColumn.setCellValueFactory(cell -> cell.getValue().surnameProperty());
        this.peselColumn.setCellValueFactory(cell -> cell.getValue().peselProperty().asObject());
        this.employmentPlace.setCellValueFactory(cell -> cell.getValue().autoRepairShopFxProperty());
    }

    public void setTreeView() {
        this.mechanicModel.initRoot();
        this.treeViewMechanic.setRoot(this.mechanicModel.getRoot());
    }

    public void treeTabisSelected(Event event) {
        ViewBuilder.showOnlyAddButtonLeft();

    }

    public void filterByCompany(ActionEvent actionEvent) {
        this.mechanicModel.filterObservableListByPredicate();
    }

    public void cancelFilter(ActionEvent actionEvent) {
        mechanicModel.initList();
        companyCombobox.getSelectionModel().clearSelection();
    }
}
