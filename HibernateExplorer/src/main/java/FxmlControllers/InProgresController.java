package FxmlControllers;

import ModelsFx.*;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.layout.VBox;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

public class InProgresController {
    private ResourceBundle resourceBundle;

    public static final String IN_PROGRES_FXML = "/Fxml/InProgres.fxml";
    @FXML
    private VBox inProgres;
    @FXML
    private TableView<RepairOrderFx> tableOrders;
    @FXML
    private TableColumn<RepairOrderFx, LocalDateTime> dateOfAddOrder;
    @FXML
    private TableColumn<RepairOrderFx, CarFx> carOrder;
    @FXML
    private TableColumn<RepairOrderFx, AutoRepairShopFx> autoRepaiOrder;
    @FXML
    private TableColumn<RepairOrderFx, MechanicFx> mechanicOrder;
    @FXML
    private TableColumn<RepairOrderFx,Boolean> transferedColumn;
    @FXML
    private TableColumn<RepairOrderFx, Boolean> endedColumn;
    @FXML
    private TableColumn<RepairOrderFx, RepairOrderFx> editColumn;

    private RepairOrderModel repairOrderModel;

    @FXML
    public void initialize() {
        this.repairOrderModel = new RepairOrderModel();
        this.repairOrderModel.initList();
        this.resourceBundle = ResourceBundle.getBundle("bundles.messages");

        setTableView();
        this.tableOrders.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                RepairOrderFx selectedItem = tableOrders.getSelectionModel().getSelectedItem();
                this.repairOrderModel.setObjectProperty(selectedItem);
            }
        });
    }

    private void setTableView() {
        ObservableList<RepairOrderFx> observableList = repairOrderModel.getObservableList();
        this.tableOrders.setItems(observableList);
        this.dateOfAddOrder.setCellValueFactory(cell -> cell.getValue().dateOfOrdeProperty());
        this.carOrder.setCellValueFactory(cell -> cell.getValue().carFxProperty());
        this.autoRepaiOrder.setCellValueFactory(cell -> cell.getValue().autoRepairShopFxProperty());
        this.mechanicOrder.setCellValueFactory(cell -> cell.getValue().mechanicFxProperty());
        this.transferedColumn.setCellFactory(CheckBoxTableCell.forTableColumn(param -> tableOrders.getItems().get(param).tranferredToMechanicProperty()));
        this.endedColumn.setCellFactory(CheckBoxTableCell.forTableColumn(param -> tableOrders.getItems().get(param).repairedProperty()));
        this.editColumn.setCellValueFactory(cell -> new SimpleObjectProperty<>(cell.getValue()));
        this.editColumn.setCellFactory(param -> new TableCell<RepairOrderFx, RepairOrderFx>(){
            Button button = createEditButton();
            @Override
            protected void updateItem(RepairOrderFx item, boolean empty) {
                super.updateItem(item, empty);
                if (!empty){
                    setGraphic(button);
                    button.setPrefWidth(200.0);
                    button.setPrefHeight(30.0);
                    button.setAlignment(Pos.CENTER);
                    button.setOnAction(event -> repairOrderModel.editOrderStatus(item));
                }
            }
        });
    }
    private Button createEditButton(){
        Button button = new Button(resourceBundle.getString("editButton"));
        return button;
    }

}
