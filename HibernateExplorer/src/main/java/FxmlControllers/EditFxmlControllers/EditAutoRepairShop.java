package FxmlControllers.EditFxmlControllers;

import Dialogs.DialogsErrorAplication;
import Dialogs.DialogsInformation;
import FxmlControllers.BorderPaneTopController;
import FxmlControllers.MyUniversalTools;
import FxmlControllers.ViewBuilder;
import ModelsFx.AutoRepairShopFx;
import ModelsFx.AutoRepairShopModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

public class EditAutoRepairShop {
    @FXML
    private VBox EditAutoRepairShop;
    @FXML
    private TextField nameAutoRepairShop;
    @FXML
    private TextField adressAutoRepairShop;
    @FXML
    private TextField nipAutoRepairShop;
    @FXML
    private Button okButton;

    private AutoRepairShopModel autoRepairShopModel;
    private AutoRepairShopFx selectedAutoRepairShopFx;

    public static final String EDIT_AUTO_REPAIR_SHOP_FXML = "/Fxml/EditFxml/EditAutoRepairShop.fxml";

    public void initialize() {
        autoRepairShopModel = new AutoRepairShopModel();
        setControls();
    }

    private void setControls() {
        selectedAutoRepairShopFx = autoRepairShopModel.objectPropertyProperty().get();
        nameAutoRepairShop.setText(selectedAutoRepairShopFx.getName());
        adressAutoRepairShop.setText(selectedAutoRepairShopFx.getAdress());
        nipAutoRepairShop.setText(String.valueOf(selectedAutoRepairShopFx.getNIP()));
    }

    public void editAutoRepairShop(ActionEvent event) {
        if (nipIsCorrect()) {
            try {
                autoRepairShopModel.updateAutoRepairShop(nameAutoRepairShop.getText(), adressAutoRepairShop.getText(), Long.parseLong(nipAutoRepairShop.getText()), selectedAutoRepairShopFx.getId());
                DialogsInformation.addSukcess();
            } catch (Exception e) {
                DialogsErrorAplication.daoExceptionDialog(e);
            }
            ViewBuilder.setCenter(BorderPaneTopController.AUTO_REPAIR_SHOP_FXML);
            ViewBuilder.clearToggleGrpupLeft();
        }
    }

    public boolean nipIsCorrect() {
        String nipToChect = nipAutoRepairShop.getText().trim();
        boolean isLong = MyUniversalTools.isLong(nipToChect);
        if (!isLong) {
            return false;
        }
        boolean isGoodLenght = MyUniversalTools.isGoodLenght(nipToChect, 10);
        if (!isGoodLenght) {
            return false;
        }
        return true;
    }
}
