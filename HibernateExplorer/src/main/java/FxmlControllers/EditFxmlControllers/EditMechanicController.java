package FxmlControllers.EditFxmlControllers;

import Dialogs.DialogsErrorAplication;
import Dialogs.DialogsInformation;
import FxmlControllers.BorderPaneTopController;
import FxmlControllers.MyUniversalTools;
import FxmlControllers.ViewBuilder;
import ModelsFx.AutoRepairShopFx;
import ModelsFx.AutoRepairShopModel;
import ModelsFx.MechanicFx;
import ModelsFx.MechanicModel;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

public class EditMechanicController {
    @FXML
    private VBox EditMechanic;
    @FXML
    private TextField name;
    @FXML
    private TextField surname;
    @FXML
    private TextField pesel;
    @FXML
    private ComboBox<AutoRepairShopFx> comboBox;
    @FXML
    private Button okButton;

    private MechanicModel mechanicModel;
    private AutoRepairShopModel autoRepairShopModel;
    private MechanicFx selectedMechanic;

    public static final String EDIT_MECHANIC_FXML = "/Fxml/EditFxml/EditMechanic.fxml";

    @FXML
    public void initialize() {
        this.mechanicModel = new MechanicModel();
        this.autoRepairShopModel = new AutoRepairShopModel();
        setControls();
    }

    public void setControls() {
        this.selectedMechanic = mechanicModel.getObjectProperty();
        name.setText(selectedMechanic.getName());
        surname.setText(selectedMechanic.getSurname());
        pesel.setText(String.valueOf(selectedMechanic.getPesel()));

        autoRepairShopModel.initList();
        ObservableList<AutoRepairShopFx> autoRepairShopModelObservableList = autoRepairShopModel.getObservableList();
        autoRepairShopModelObservableList.add(null);
        comboBox.setItems(autoRepairShopModel.getObservableList());
        comboBox.setValue(selectedMechanic.getAutoRepairShopFx());
    }

    public void editMechanic(ActionEvent event) {
        if (peselIsCorrect()) {
            long peselNumber = Long.parseLong(pesel.getText());
            try {
                mechanicModel.editMechanic(peselNumber, name.getText(), surname.getText(), comboBox.getValue(), selectedMechanic.getId());
                DialogsInformation.addSukcess();
            } catch (Exception e) {
                DialogsErrorAplication.daoExceptionDialog(e);
            }
            ViewBuilder.setCenter(BorderPaneTopController.MECHANICS_FXML);
        }
    }

    public boolean peselIsCorrect() {
        String peselToChect = pesel.getText().trim();
        boolean isLong = MyUniversalTools.isLong(peselToChect);
        if (!isLong) {
            return false;
        }
        boolean isGoodLenght = MyUniversalTools.isGoodLenght(peselToChect, 11);
        if (!isGoodLenght) {
            return false;
        }
        return true;
    }
}
