package FxmlControllers.EditFxmlControllers;

import ModelsSQL.Models.CarBodyType;
import Dialogs.DialogsErrorAplication;
import Dialogs.DialogsInformation;
import FxmlControllers.BorderPaneTopController;
import FxmlControllers.ViewBuilder;
import ModelsFx.*;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

public class EditCarController {

    @FXML
    private VBox EditCar;
    @FXML
    private TextField registrationNumber;
    @FXML
    private TextField brand;
    @FXML
    private TextField model;
    @FXML
    private DatePicker dateOfProduction;
    @FXML
    private TextField km;
    @FXML
    private ComboBox<String> carBodyType;
    @FXML
    private TextField color;
    @FXML
    private Button okButton;

    public static final String EDIT_CAR_FXML = "/Fxml/EditFxml/EditCar.fxml";

    private CarModel carModel;
    private CarFx selectedCarFx;
    private CarBodyTypeModel carBodyTypeModel;

    @FXML
    public void initialize() {
        this.carModel = new CarModel();
        this.carBodyTypeModel = new CarBodyTypeModel();
        setControls();
    }

    public void setControls() {
        this.selectedCarFx = carModel.objectPropertyProperty().get();
        this.registrationNumber.setText(selectedCarFx.getRegistrationNumber());
        this.brand.setText(selectedCarFx.getBrand());
        this.model.setText(selectedCarFx.getModel());
        this.km.setText(String.valueOf(selectedCarFx.getKm()));
        this.color.setText(selectedCarFx.getColor());

        this.carBodyTypeModel.initList();
        this.carBodyType.setItems(carBodyTypeModel.getObservableList());
        this.carBodyType.setValue(String.valueOf(selectedCarFx.getCarBodyType()));

        this.dateOfProduction.setValue(selectedCarFx.getProductionDate());

    }


    public void editCar(ActionEvent event) {
        if (isLong(km.getText())) {
            try {
                carModel.updateCar(registrationNumber.getText(), brand.getText(), model.getText(), CarBodyType.valueOf(carBodyType.getValue()), dateOfProduction.getValue(), color.getText(), Long.parseLong(km.getText()), selectedCarFx.getId());
                DialogsInformation.addSukcess();
            } catch (Exception e) {
                DialogsErrorAplication.daoExceptionDialog(e);
            }
            ViewBuilder.setCenter(BorderPaneTopController.CARS_FXML);
            ViewBuilder.clearToggleGrpupLeft();
        }
    }

    public boolean isLong(String valueToParse) {
        try {
            Long.parseLong(valueToParse);
            return true;
        } catch (NumberFormatException e) {
            DialogsErrorAplication.LongParseExceptionDialog(valueToParse);
            return false;
        }
    }
}
