package FxmlControllers.EditFxmlControllers;

import ModelsFx.RepairOrderModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.BorderPane;

public class EditStatus {

    @FXML
    private BorderPane editStatus;
    @FXML
    private CheckBox mechanicCheckBox;

    private RepairOrderModel repairOrderModel;
    @FXML
    public void initialize(){
        this.repairOrderModel = new RepairOrderModel();
    }

    public void okClick(ActionEvent actionEvent) {
    }

    public void CancelClick(ActionEvent actionEvent) {
    }

    public void transferToMechanic(ActionEvent actionEvent) {
    }

    public void markAsFinished(ActionEvent actionEvent) {
    }

    public void cancelOrder(ActionEvent actionEvent) {
    }
}
