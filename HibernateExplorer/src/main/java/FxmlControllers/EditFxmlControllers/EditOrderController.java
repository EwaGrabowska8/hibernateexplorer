package FxmlControllers.EditFxmlControllers;

import Dialogs.DialogsErrorAplication;
import Dialogs.DialogsInformation;
import FxmlControllers.BorderPaneTopController;
import FxmlControllers.ViewBuilder;
import ModelsFx.*;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;

public class EditOrderController {

    public static final String EDIT_ORDER_FXML = "/Fxml/EditFxml/EditOrder.fxml";

    @FXML
    private VBox EditOrder;
    @FXML
    private ComboBox<CarFx> comboBoxCar;
    @FXML
    private ComboBox<AutoRepairShopFx> comboBoxAutoRepairShop;
    @FXML
    private ComboBox<MechanicFx> comboBoxMechanic;
    @FXML
    private TextArea textAreaDescription;
    @FXML
    private Button okButton;

    private RepairOrderModel repairOrderModel;
    private CarModel carModel;
    private AutoRepairShopModel autoRepairShopModel;
    private MechanicModel mechanicModel;

    private RepairOrderFx selectedRepairOrder;

    @FXML
    public void initialize() {
        this.repairOrderModel = new RepairOrderModel();
        this.carModel = new CarModel();
        this.autoRepairShopModel = new AutoRepairShopModel();
        this.mechanicModel = new MechanicModel();

        setControls();
    }

    private void setControls() {
        this.selectedRepairOrder = repairOrderModel.getObjectProperty();
        System.err.println(selectedRepairOrder);
        textAreaDescription.setText(selectedRepairOrder.getDescription());

        carModel.initList();
        ObservableList<CarFx> carModelObservableList = carModel.getObservableList();
        carModelObservableList.add(null);
        comboBoxCar.setItems(carModelObservableList);
        comboBoxCar.setValue(selectedRepairOrder.getCarFx());

        autoRepairShopModel.initList();
        ObservableList<AutoRepairShopFx> autoRepairShopModelObservableList = autoRepairShopModel.getObservableList();
        autoRepairShopModelObservableList.add(null);
        comboBoxAutoRepairShop.setItems(autoRepairShopModelObservableList);
        comboBoxAutoRepairShop.setValue(selectedRepairOrder.getAutoRepairShopFx());

        mechanicModel.initList();
        ObservableList<MechanicFx> mechanicModelObservableList = mechanicModel.getObservableList();
        mechanicModelObservableList.add(null);
        comboBoxMechanic.setItems(mechanicModelObservableList);
        comboBoxMechanic.setValue(selectedRepairOrder.getMechanicFx());
    }

    public void addOrder(ActionEvent event) {
        try {
            repairOrderModel.updateRepairOrder
                    (textAreaDescription.getText(), comboBoxCar.getValue(), comboBoxAutoRepairShop.getValue(), comboBoxMechanic.getValue(), selectedRepairOrder.getId());
            DialogsInformation.addSukcess();
        } catch (Exception e) {
            DialogsErrorAplication.daoExceptionDialog(e);
        }
        ViewBuilder.setCenter(BorderPaneTopController.ORDERS_FXML);
        ViewBuilder.clearToggleGrpupLeft();
    }
}
