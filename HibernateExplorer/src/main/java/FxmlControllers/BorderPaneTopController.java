package FxmlControllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.HBox;

import java.io.IOException;
import java.util.Locale;

public class BorderPaneTopController {

    public static final String AUTO_REPAIR_SHOP_FXML = "/Fxml/AutoRepairShop.fxml";
    public static final String MECHANICS_FXML = "/Fxml/Mechanics.fxml";
    public static final String CARS_FXML = "/Fxml/Cars.fxml";
    public static final String ORDERS_FXML = "/Fxml/Orders.fxml";
    public static final String ARCHIVES_FXML = "/Fxml/Archives.fxml";

    @FXML
    private HBox borderPaneTop;
    @FXML
    private ComboBox<Locale> language;
    private MainController mainController;
    private BorderPaneLeftController borderPaneLeft;

    @FXML
    void initialize(){
        this.setChoiceBox();
    }

    private void setChoiceBox() {
        language.getItems().addAll(Locale.ENGLISH, new Locale("pl"));
        language.setValue(Locale.getDefault());
        language.valueProperty().addListener((obs, oldValue, newValue) -> {
            if (newValue != null) {
                Locale.setDefault(newValue);
                ViewBuilder.reloadLanguage();
            }
        });
    }

    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    public void setBorderPaneLeft(BorderPaneLeftController borderPaneLeft) {
        this.borderPaneLeft = borderPaneLeft;
    }

    @FXML
    public void setLeftController(ActionEvent event) {
        borderPaneLeft.setTop((ToggleButton) event.getSource());
        borderPaneLeft.showOnlyAdd();
        borderPaneLeft.clear();
    }

    @FXML
    public void openAutoRepairSchopsTB(ActionEvent event) throws IOException {
        ViewBuilder.setCenter(AUTO_REPAIR_SHOP_FXML);
        setLeftController(event);
    }

    @FXML
    public void openMechanicsTB(ActionEvent event) throws IOException {
        ViewBuilder.setCenter(MECHANICS_FXML);
        setLeftController(event);
    }

    @FXML
    public void openCarsTB(ActionEvent event) throws IOException {
        ViewBuilder.setCenter(CARS_FXML);
        setLeftController(event);
    }

    @FXML
    public void openOrdersTB(ActionEvent event) throws IOException {
        ViewBuilder.setCenter(ORDERS_FXML);
        setLeftController(event);
    }

    @FXML
    public void openUstercar(ActionEvent event) {
        borderPaneLeft.setTop((ToggleButton) event.getSource());
        borderPaneLeft.haidButtons();
        borderPaneLeft.clear();
    }

    public void openAtchives(ActionEvent event) {
        ViewBuilder.setCenter(ARCHIVES_FXML);
        borderPaneLeft.haidButtons();
        borderPaneLeft.clear();
    }

    public void inProgresTB(ActionEvent actionEvent) {
        ViewBuilder.setCenter(InProgresController.IN_PROGRES_FXML);
        ViewBuilder.hideAllButtonLeft();
    }
}
