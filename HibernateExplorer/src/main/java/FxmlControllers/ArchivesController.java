package FxmlControllers;


import ModelsFx.*;
import ModelsSQL.Models.CarBodyType;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.VBox;

import java.time.LocalDate;

public class ArchivesController {
    @FXML
    private VBox Archives;
    @FXML
    private TableView<AutoRepairShopFx> tableAutoRepairShop;
    @FXML
    private TableColumn<CarFx, String> registrationNumberColumn;
    @FXML
    private TableColumn<AutoRepairShopFx, String> nameAutoRepairShopColumn;
    @FXML
    private TableColumn<AutoRepairShopFx, String> adressAutoRepairShopColumn;
    @FXML
    private TableColumn<AutoRepairShopFx, Long> nipColumn;

    @FXML
    private TableView<MechanicFx> tableMechanic;
    @FXML
    private TableColumn<MechanicFx, String> nameColumn;
    @FXML
    private TableColumn<MechanicFx, String> surnameColumn;
    @FXML
    private TableColumn<MechanicFx, Long> peselColumn;
    @FXML
    private TableColumn<MechanicFx, AutoRepairShopFx> employmentPlace;

    @FXML
    private TableView<CarFx> tableCar;
    @FXML
    private TableColumn<CarFx, String> brandColumn;
    @FXML
    private TableColumn<CarFx, String> modelColumn;
    @FXML
    private TableColumn<CarFx, LocalDate> productionDateColumn;
    @FXML
    private TableColumn<CarFx, Long> kmColumn;
    @FXML
    private TableColumn<CarFx, CarBodyType> carBodyTypeColumn;
    @FXML
    private TableColumn<CarFx, String> colorColumn;
    @FXML
    private TableColumn<CarFx, MechanicFx> mechanicColumn;

    private AutoRepairShopModel autoRepairShopModel;
    private MechanicModel mechanicModel;
    private CarModel carModel;

    @FXML
    public void initialize() {
        this.autoRepairShopModel = new AutoRepairShopModel();
        this.mechanicModel = new MechanicModel();
        this.carModel = new CarModel();

        setTabAutoRepairShop();
        setTabMechanic();
        setTabCar();
    }

    private void setTabCar() {
        this.carModel.initList();
        ObservableList<CarFx> observableList = carModel.getObservableListArchive();
        this.tableCar.setItems(observableList);
        this.registrationNumberColumn.setCellValueFactory(cell -> cell.getValue().registrationNumberProperty());
        this.brandColumn.setCellValueFactory(cell -> cell.getValue().brandProperty());
        this.modelColumn.setCellValueFactory(cell -> cell.getValue().modelProperty());
        this.productionDateColumn.setCellValueFactory(cell -> cell.getValue().productionDateProperty());
        this.kmColumn.setCellValueFactory(cell -> cell.getValue().kmProperty());
        this.carBodyTypeColumn.setCellValueFactory(cell -> cell.getValue().carBodyTypeProperty());
        this.colorColumn.setCellValueFactory(cell -> cell.getValue().colorProperty());
        this.mechanicColumn.setCellValueFactory(cell -> cell.getValue().mechanicProperty());
        this.tableCar.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                CarFx selectedItem = tableCar.getSelectionModel().getSelectedItem();
                this.carModel.setObjectPropertyArchive(selectedItem);
            }
        });

    }

    private void setTabMechanic() {
        this.mechanicModel.initList();
        ObservableList<MechanicFx> observableList = mechanicModel.getObservableListArchive();
        this.tableMechanic.setItems(observableList);
        this.nameColumn.setCellValueFactory(cell -> cell.getValue().nameProperty());
        this.surnameColumn.setCellValueFactory(cell -> cell.getValue().surnameProperty());
        this.peselColumn.setCellValueFactory(cell -> cell.getValue().peselProperty().asObject());
        this.employmentPlace.setCellValueFactory(cell -> cell.getValue().autoRepairShopFxProperty());
        this.tableMechanic.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                MechanicFx selectedItem = tableMechanic.getSelectionModel().getSelectedItem();
                this.mechanicModel.setObjectPropertyArchive(selectedItem);
            }
        });
    }

    public void setTabAutoRepairShop() {
        this.autoRepairShopModel.initList();
        this.tableAutoRepairShop.setItems(autoRepairShopModel.getObservableListArchive());
        this.nameAutoRepairShopColumn.setCellValueFactory(cell -> cell.getValue().nameProperty());
        this.adressAutoRepairShopColumn.setCellValueFactory(cell -> cell.getValue().adressProperty());
        this.nipColumn.setCellValueFactory(cell -> cell.getValue().NIPProperty().asObject());
        this.tableAutoRepairShop.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                AutoRepairShopFx selectedItem = tableAutoRepairShop.getSelectionModel().getSelectedItem();
                this.autoRepairShopModel.setObjectPropertyArchive(selectedItem);
            }
        });
    }

}
