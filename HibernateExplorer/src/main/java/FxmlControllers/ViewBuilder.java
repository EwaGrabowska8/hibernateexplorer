package FxmlControllers;

import ModelsFx.RepairOrderFx;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ResourceBundle;

public class ViewBuilder {
    private static BorderPane mainBorderPane;
    private static BorderPaneLeftController borderPaneLeftController;

    public static void setCenter(String resourcesPath) {
        Parent parent = getParent(resourcesPath);
        mainBorderPane.setCenter(parent);
    }

    private static Parent getParent(String resourcesPath) {
        FXMLLoader loader = new FXMLLoader(ViewBuilder.class.getResource(resourcesPath));
        ResourceBundle resourceBundle = ResourceBundle.getBundle("bundles.messages");
        loader.setResources(resourceBundle);
        Parent parent = null;
        try {
            parent = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return parent;
    }

    public static void reloadLanguage(){
        ResourceBundle resourceBundle = ResourceBundle.getBundle("bundles.messages");
        Stage stage = (Stage) mainBorderPane.getScene().getWindow();
        Parent parent = getParent("/Fxml/Main.fxml");
        stage.setScene(new Scene(parent));
        stage.setTitle(resourceBundle.getString("title"));
        stage.show();
    }


    public static void setMainBorderPane(BorderPane mainBorderPane) {
        ViewBuilder.mainBorderPane = mainBorderPane;
    }

    public static void setBorderPaneLeft(BorderPaneLeftController borderPaneLeft) {
        ViewBuilder.borderPaneLeftController = borderPaneLeft;
    }

    public static void clearToggleGrpupLeft() {
        borderPaneLeftController.setToggleGroupLeft();
    }

    public static void showOnlyAddButtonLeft() {
        borderPaneLeftController.showOnlyAdd();
    }

    public static void showAllButtonLeft() {
        borderPaneLeftController.showButtons();
    }

    public static void hideAllButtonLeft() {
        borderPaneLeftController.haidButtons();
    }

    public static void hideOnlyButtonArchive() {
        borderPaneLeftController.haidOnlyArchive();
    }

    public static void setModalWindowEditStatus(RepairOrderFx item) {
        Parent parent = getParent("/Fxml/EditFxml/EditStatus.fxml");
        Scene scene = new Scene(parent);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.getIcons().add(new Image("/icons/wheel.png"));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
    }
}
