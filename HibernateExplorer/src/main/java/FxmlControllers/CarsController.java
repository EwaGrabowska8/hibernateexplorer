package FxmlControllers;

import ModelsSQL.Models.CarBodyType;
import ModelsFx.CarFx;
import ModelsFx.CarModel;
import ModelsFx.MechanicFx;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeView;
import javafx.scene.layout.VBox;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class CarsController {
    @FXML
    private VBox cars;
    @FXML
    private Tab tableTabCar;
    @FXML
    private TableView<CarFx> tableCar;
    @FXML
    private TableColumn<CarFx, String> registrationNumberColumn;
    @FXML
    private TableColumn<CarFx, String> brandColumn;
    @FXML
    private TableColumn<CarFx, String> modelColumn;
    @FXML
    private TableColumn<CarFx, LocalDate> productionDateColumn;
    @FXML
    private TableColumn<CarFx, Long> kmColumn;
    @FXML
    private TableColumn<CarFx, CarBodyType> carBodyTypeColumn;
    @FXML
    private TableColumn<CarFx, String> colorColumn;
    @FXML
    private TableColumn<CarFx, MechanicFx> mechanicColumn;
    @FXML
    private TreeView<String> treeViewCar;
    @FXML
    private TableColumn<CarFx, LocalDateTime> lastModify;
    @FXML
    private Tab treeTabCar;

    private CarModel carModel;

    @FXML
    public void initialize() {
        this.carModel = new CarModel();

        this.carModel.initList();
        setTreeView();
        setTableView();
        this.tableCar.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                CarFx selectedItem = tableCar.getSelectionModel().getSelectedItem();
                this.carModel.setObjectProperty(selectedItem);
                ViewBuilder.showAllButtonLeft();
            }
        });
        ViewBuilder.showOnlyAddButtonLeft();
    }

    public void setTableView() {
        ObservableList<CarFx> observableList = carModel.getObservableList();
        this.tableCar.setItems(observableList);
        this.registrationNumberColumn.setCellValueFactory(cell -> cell.getValue().registrationNumberProperty());
        this.brandColumn.setCellValueFactory(cell -> cell.getValue().brandProperty());
        this.modelColumn.setCellValueFactory(cell -> cell.getValue().modelProperty());
        this.productionDateColumn.setCellValueFactory(cell -> cell.getValue().productionDateProperty());
        this.kmColumn.setCellValueFactory(cell -> cell.getValue().kmProperty());
        this.carBodyTypeColumn.setCellValueFactory(cell -> cell.getValue().carBodyTypeProperty());
        this.colorColumn.setCellValueFactory(cell -> cell.getValue().colorProperty());
        this.mechanicColumn.setCellValueFactory(cell -> cell.getValue().mechanicProperty());
        this.lastModify.setCellValueFactory(cell->cell.getValue().modifiedDateProperty());
    }

    public void setTreeView() {
        this.carModel.initRoot();
        this.treeViewCar.setRoot(this.carModel.getRoot());
    }

    public void treeTabisSelected(Event event) {
        ViewBuilder.showOnlyAddButtonLeft();

    }
}
