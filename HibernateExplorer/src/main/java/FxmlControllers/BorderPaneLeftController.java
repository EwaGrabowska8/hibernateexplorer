package FxmlControllers;

import ModelsSQL.Models.AutoRepairShopSQL;
import ModelsSQL.Models.CarSQL;
import ModelsSQL.Models.MechanicSQL;
import Dialogs.DialogsConfirm;
import Dialogs.DialogsErrorAplication;
import Dialogs.DialogsInformation;
import FxmlControllers.AddFxmlControllers.AddAutoRepairShopController;
import FxmlControllers.AddFxmlControllers.AddCarController;
import FxmlControllers.AddFxmlControllers.AddMechanicController;
import FxmlControllers.AddFxmlControllers.AddOrderController;
import FxmlControllers.EditFxmlControllers.EditAutoRepairShop;
import FxmlControllers.EditFxmlControllers.EditCarController;
import FxmlControllers.EditFxmlControllers.EditMechanicController;
import FxmlControllers.EditFxmlControllers.EditOrderController;
import ModelsFx.AutoRepairShopModel;
import ModelsFx.CarModel;
import ModelsFx.MechanicModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;

import java.util.Optional;

public class BorderPaneLeftController {
    @FXML
    private VBox borderPaneLeft;
    @FXML
    private ToggleGroup left;

    private ToggleButton top;
    private ToggleButton leftButton;


    public void setTop(ToggleButton top) {
        this.top = top;
    }

    public void setToggleGroupLeft() {
        leftButton.setSelected(false);
    }

    public void add(ActionEvent event) {
        leftButton = (ToggleButton) event.getSource();
        switch (top.getId()) {
            case "autoRepairShopsTB": {
                ViewBuilder.setCenter(AddAutoRepairShopController.ADD_AUTO_REPAIR_SHOP_FXML);
                break;
            }
            case "mechanicsTB": {
                ViewBuilder.setCenter(AddMechanicController.ADD_MECHANIC_FXML);
                break;
            }
            case "carsTB": {
                ViewBuilder.setCenter(AddCarController.ADD_CAR_FXML);
                break;
            }
            case "ordersTB": {
                ViewBuilder.setCenter(AddOrderController.ADD_ORDER_FXML);
                break;
            }
        }
    }

    public void archive(ActionEvent event) {
        leftButton = (ToggleButton) event.getSource();
        switch (top.getId()) {
            case "autoRepairShopsTB": {
                Optional<ButtonType> result = DialogsConfirm.archivalStatusDialog(AutoRepairShopSQL.class);
                ButtonType buttonType = result.get();
                if (buttonType == ButtonType.OK) {
                    try {
                        AutoRepairShopModel autoRepairShopModel = new AutoRepairShopModel();
                        autoRepairShopModel.setIsArchiveAndRemoveRelation();
                        DialogsInformation.addSukcess();
                    } catch (Exception e) {
                        DialogsErrorAplication.daoExceptionDialog(e);
                    }
                    ViewBuilder.setCenter(BorderPaneTopController.AUTO_REPAIR_SHOP_FXML);
                }
                break;
            }
            case "mechanicsTB": {
                Optional<ButtonType> result = DialogsConfirm.archivalStatusDialog(MechanicSQL.class);
                ButtonType buttonType = result.get();
                if (buttonType == ButtonType.OK) {
                    try {
                        MechanicModel mechanicModel = new MechanicModel();
                        mechanicModel.setIsArchiveAndRemoveRelation();
                        DialogsInformation.addSukcess();
                    } catch (Exception e) {
                        DialogsErrorAplication.daoExceptionDialog(e);
                    }
                    ViewBuilder.setCenter(BorderPaneTopController.MECHANICS_FXML);
                }
                break;
            }
            case "carsTB": {
                Optional<ButtonType> result = DialogsConfirm.archivalStatusDialog(CarSQL.class);
                ButtonType buttonType = result.get();
                if (buttonType == ButtonType.OK) {
                    try {
                        CarModel carModel = new CarModel();
                        carModel.setIsArchiveAndRemoveRelation();
                        DialogsInformation.addSukcess();
                    } catch (Exception e) {
                        DialogsErrorAplication.daoExceptionDialog(e);
                    }
                    ViewBuilder.setCenter(BorderPaneTopController.CARS_FXML);
                }
                break;
            }
        }
    }

    public void edit(ActionEvent event) {
        leftButton = (ToggleButton) event.getSource();
        switch (top.getId()) {
            case "autoRepairShopsTB": {
                ViewBuilder.setCenter(EditAutoRepairShop.EDIT_AUTO_REPAIR_SHOP_FXML);
                break;
            }
            case "mechanicsTB": {
                ViewBuilder.setCenter(EditMechanicController.EDIT_MECHANIC_FXML);
                break;
            }
            case "carsTB": {
                ViewBuilder.setCenter(EditCarController.EDIT_CAR_FXML);
                break;
            }
            case "ordersTB": {
                ViewBuilder.setCenter(EditOrderController.EDIT_ORDER_FXML);
                break;
            }
        }

    }

    public void showDetails(ActionEvent event) {
        leftButton = (ToggleButton) event.getSource();
    }

    public void clear() {
        if (leftButton != null) {
            leftButton.setSelected(false);
            leftButton = null;
        }
    }

    public void haidButtons() {
        left.getToggles().forEach(toggle -> {
            Node node = (Node) toggle;
            node.setDisable(true);
        });
    }

    public void showButtons() {
        left.getToggles().forEach(toggle -> {
            Node node = (Node) toggle;
            node.setDisable(false);
        });
    }

    public void showOnlyAdd() {
        haidButtons();
        Node addButtonNode = (Node) left.getToggles().get(0);
        addButtonNode.setDisable(false);

    }

    public void haidOnlyArchive() {
        showButtons();
        Node archiveButtonNode = (Node) left.getToggles().get(1);
        archiveButtonNode.setDisable(true);
    }
}
